#include <dirent.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include "lvgl/lvgl.h"
#include "bookmenu.h"
#include "bookviewer.h"
#include "statusbar.h"
#include "input.h"
#include "display.h"
#include "config_parser.h"
#include "logger.h"

#define PIPEBUFSIZE 128

static void focusedEventCb(lv_event_t * e);
static void defocusedEventCb(lv_event_t * e);
static void pressedBookEventCb(lv_event_t * e);
void freezeBookpath(void);

void setRelativeBookpath(char *relativePath);
bool newBooksAvailable(void);
void downloadNewBooks(void);

static lv_style_t header_style;
static lv_style_t focused_style;
static lv_style_t unfocused_style;
static lv_style_t zeroPaddingStyle;

char bookpath[NAME_MAX];
bool bookpathChangeable = true;

lv_obj_t * bookMenuTitle;
lv_obj_t *booklist;
void makeBookLabels(void);
void downloadTimerCb(lv_timer_t *timer);
bool downloading = false;

void makeBookMenu(void)
{
    lv_style_init(&zeroPaddingStyle);

    lv_style_set_pad_top(&zeroPaddingStyle, 0);
    lv_style_set_pad_left(&zeroPaddingStyle, 0);
    lv_style_set_pad_bottom(&zeroPaddingStyle, 0);
    lv_style_set_pad_right(&zeroPaddingStyle, 0);

    // make a container object
    lv_obj_t *containerColumn = lv_obj_create(lv_scr_act());
    lv_obj_set_size(containerColumn, lv_pct(100), lv_pct(100));
    lv_obj_set_flex_flow(containerColumn, LV_FLEX_FLOW_COLUMN);
    lv_obj_add_style(containerColumn, &zeroPaddingStyle, 0);

    lv_obj_add_style(makeStatusbar(containerColumn), &zeroPaddingStyle, 0);

    // Make the screen title
    lv_style_init(&header_style);
    lv_style_set_text_font(&header_style, &lv_font_montserrat_34);
    bookMenuTitle = lv_label_create(containerColumn);
    lv_obj_set_width(bookMenuTitle, lv_pct(100));
    if (newBooksAvailable()){
        downloadNewBooks();
        lv_timer_t *downloadingTimer = lv_timer_create(downloadTimerCb, 5000, NULL);
        lv_timer_ready(downloadingTimer);
        lv_label_set_text(bookMenuTitle, LV_SYMBOL_DOWNLOAD "Select A Book");
    }else{
        lv_label_set_text(bookMenuTitle, "Select A Book");
    }
    lv_obj_add_style(bookMenuTitle, &header_style, 0);
    lv_obj_set_style_text_align(bookMenuTitle, LV_TEXT_ALIGN_CENTER, 0);
    lv_obj_center(bookMenuTitle);
    lv_obj_add_style(bookMenuTitle, &zeroPaddingStyle, 0);

    // intialize book title styles
    lv_style_init(&focused_style);
    lv_style_init(&unfocused_style);
    #ifdef TOUCH
        lv_style_set_text_font(&focused_style, &lv_font_montserrat_24);
        lv_style_set_text_font(&unfocused_style, &lv_font_montserrat_24);
    #endif
    lv_style_set_text_color(&focused_style, lv_color_white());
    lv_style_set_text_color(&unfocused_style, lv_color_black());

	// make container for book titles
	booklist = lv_obj_create(containerColumn);
	lv_obj_set_size(booklist, lv_pct(100), lv_pct(100));
    lv_obj_set_flex_flow(booklist, LV_FLEX_FLOW_COLUMN);
    lv_obj_add_style(booklist, &zeroPaddingStyle, 0);

    // make book title labels
    makeBookLabels();
}

void downloadNewBooks(void)
{
	logDebug("Downloading new books");
    char *cmd = getenv("ER_DOWNLOAD_BOOKS_EXEC");
    if (cmd == NULL)
    {
        logError("Command to download books is undefined.");
        return;
    }
    system(cmd);
    downloading = true;
}

bool newBooksAvailable(void)
{
    char *cmd = getenv("ER_CHECK_AVAILABLE_BOOKS_EXEC");
    if (cmd == NULL)
    {
        logError("Command to check remote books is undefined.");
        return false;
    }
    char *output = "";
    char buf[PIPEBUFSIZE] = {0};
    FILE *fp;

    if ((fp = popen(cmd, "r")) == NULL) {
        logError("Error opening pipe");
        return false;
    }

    while (fgets(buf, PIPEBUFSIZE, fp) != NULL) {
        output = buf;
    }

    if (pclose(fp)) {
        logError("Command not found or exited with error status");
        return false;
    }

    if (strcmp(output, "1"))
    {
		downloading = false;
        return false;
    } else {
        return true;
    }
}

void makeBookLabels(void)
{
    DIR *d;
    struct dirent *dir;
    char bookDir[NAME_MAX];
    getBookDirectory(bookDir);
    d = opendir(bookDir);
    if (d)
    {
        lv_group_t *g = lvGetInputGroup();
        while ((dir = readdir(d)) != NULL)
        {
            if (strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0) continue;
            lv_obj_t * label = lv_label_create(booklist);
            #ifdef TOUCH
                lv_obj_add_style(label, &unfocused_style, 0);
                lv_obj_add_flag(label, LV_OBJ_FLAG_CLICKABLE);
                lv_group_add_obj(g, label);
            #endif
            lv_label_set_text(label, dir->d_name);
            lv_label_set_long_mode(label, LV_LABEL_LONG_WRAP);
            lv_obj_set_width(label, lv_pct(100));
            lv_obj_set_align(label, LV_ALIGN_TOP_MID);
            lv_obj_set_style_text_align(label, LV_TEXT_ALIGN_LEFT, 0);
            lv_obj_add_event_cb(label, focusedEventCb, LV_EVENT_FOCUSED, NULL);
            lv_obj_add_event_cb(label, defocusedEventCb, LV_EVENT_DEFOCUSED, NULL);
            lv_obj_add_event_cb(label, pressedBookEventCb, LV_EVENT_PRESSED, NULL);
        }
        closedir(d);
    }
}

void downloadTimerCb(lv_timer_t *timer){
	logDebug("Booklist refresh function called");
	if (newBooksAvailable() && !downloading)
	{
		logDebug("Going to start downloading");
		lv_label_set_text(bookMenuTitle, LV_SYMBOL_DOWNLOAD "Select A Book");
		downloadNewBooks();
	} else {
		lv_timer_del(timer);
		logDebug("Deleting download timer");
		lv_label_set_text(bookMenuTitle, "Select A Book");
		lv_obj_clean(booklist);
		makeBookLabels();
	}
}

static void focusedEventCb(lv_event_t * e)
{
    lv_obj_t * label = lv_event_get_target(e);
    lv_obj_add_style(label, &focused_style, 0);
    lv_obj_set_style_bg_color(label, lv_color_black(), LV_PART_MAIN | LV_PART_SELECTED);
    char *label_text = lv_label_get_text(label);
    lv_label_set_text_sel_start(label, 0);
    lv_label_set_text_sel_end(label, strlen(label_text));

    if (bookpathChangeable)
    {
        setRelativeBookpath(label_text);
    }
}

static void defocusedEventCb(lv_event_t * e)
{
    lv_obj_t * label = lv_event_get_target(e);
    lv_obj_add_style(label, &unfocused_style, 0);
    lv_label_set_text_sel_start(label, 0);
    lv_label_set_text_sel_end(label, 0);
}

static void pressedBookEventCb(lv_event_t * e)
{
    logDebug("Pressed book function called");
    #ifdef TOUCH
        lv_group_focus_obj(lv_event_get_target(e));
    #else
        (void) e;
    #endif
    freezeBookpath();
    lv_obj_clean(lv_scr_act()); // delete everything on screen
    makeBookImage();
}

void freezeBookpath(void)
{
    bookpathChangeable = false;
}

void unfreezeBookpath(void)
{
    bookpathChangeable = true;
}

void getBookpath(char *buffer)
{
    strcpy(buffer, bookpath);
}

void setRelativeBookpath(char *relativePath)
{
    getBookDirectory(bookpath);
    strcat(bookpath, relativePath);
    logDebug("Focused book is: %s", bookpath);
}

void check_book_catalog(void)
{
	logDebug("restarting downloading Timer");
	lv_timer_t *downloadingTimer = lv_timer_create(downloadTimerCb, 5000, NULL);
    lv_timer_ready(downloadingTimer);
	logDebug("set download timer callback");
}
