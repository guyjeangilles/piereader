#ifndef _RPI_DEF_H_
#define _RPI_DEF_H_

#include <stdint.h>
#include <stdbool.h>

#define RPI_GPIO_INPUT 0
#define RPI_GPIO_OUTPUT 1

#define RPI_I2C_REASON_OK 0x00
#define RPI_I2C_REASON_ERROR_NACK 0x01
#define RPI_I2C_REASON_ERROR_CLKT 0x02
#define RPI_I2C_REASON_ERROR_DATA 0x04
#define RPI_I2C_REASON_ERROR_TIMEOUT 0x08
#define RPI_I2C_ERROR_UNKNOWN 0xFF

#define RPI_SPI_LSBFIRST 0
#define RPI_SPI_MSBFIRST 1
#define RPI_SPI_CS0 0
#define RPI_SPI_CS1 1
#define RPI_SPI_CLOCK_DIVIDER_128 128
#define RPI_SPI_CLOCK_DIVIDER_512 512
#define RPI_SPI_MODE0 0b00
#define RPI_SPI_MODE1 0b01
#define RPI_SPI_MODE2 0b10
#define RPI_SPI_MODE3 0b11

#define RPI_PWM_CLOCK_DIVIDER_32 32
#define RPI_PWM_CLOCK_DIVIDER_16 16

bool rpiGpioInit(void);
bool rpiGpioClose(void);
void rpiGpioSetMode(uint8_t pin, uint8_t mode);
void rpiGpioWrite(uint8_t pin, uint8_t value);
bool rpiGpioRead(uint8_t pin);
void rpiGpioPullDown(uint8_t pin);
void rpiDelay(uint32_t ms);

bool rpiI2Cinit(void);
void rpiI2CsetTargetAddress(uint8_t address);
void rpiI2CsetBaudrate(uint32_t baudrate);
uint8_t rpiI2Cwrite(char const *buffer, uint32_t len);
uint8_t rpiI2Cread(char *buffer, uint32_t len);
void rpiI2Cclose(void);

bool rpiSpiOpen(void);
void rpiSpiClose(void);
void rpiSpiSetBitOrder(uint8_t order);
void rpiSpiSetMode(uint8_t mode);
void rpiSpiSetFrequency(uint16_t divider); // argument is clock divder that sets frequency
void rpiSpiSetChipSelect(uint8_t cs);
void rpiSpiSetChipSelectPolarity(uint8_t cs, uint8_t active);
uint8_t rpiSpiTransfer(uint8_t data);
void rpiSpiTransferN(uint8_t * buffer);

void rpiPWMsetValue(uint8_t channel, uint32_t value);
void rpiPWMenableGPIO12(void);
void rpiPWMsetClock(uint16_t clock_divider);
void rpiPWMsetMode(uint8_t channel, uint8_t useMarkspace, uint8_t enabled);
void rpiPWMsetRange(uint8_t channel, uint32_t range);

#endif //_RPI_DEF_H_
