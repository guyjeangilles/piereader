#include <signal.h>
#include <stdlib.h>
#include "lvgl/lvgl.h"
#include "input.h"
#include "gpio.h"
#include "logger.h"
#include "display.h"
#include "bookmenu.h"
#include "backlight.h"

void signalHandler(int signalNumber)
{
    logInfo("Safely closing ereader(%d)", signalNumber);
    displaySafeClose();
    if(!gpioClose())
	{
		logError("Failed to cleanly close GPIO.");
		exit(1);
	}
    exit(0);
}

int32_t main(void)
{
    // Exception handling:ctrl + c
    logInfo("Starting Ereader...");
    signal(SIGINT, signalHandler);

    logInfo("Initialziing GPIO.");
    gpioInit();

    // LVGL initialization
    logInfo("Initializing LVGL");
    lv_init();
    logInfo("Initializing Display");
    lvDisplayInit();
    logInfo("Registering Display");
    lvRegisterDisplay();
    logInfo("Initializing input");
    lvInputSetup();
    logInfo("Initializing backlight");
    lvKBacklightKeypadSetup();

    logInfo("Building UI");
    makeBookMenu();
    while (1)
    {
        lv_task_handler();
        lv_tick_inc(1);
        delayMs(1);
    }
	return 0;
}
