#include <bcm2835.h>
#include "raspberrypi.h"

u_int8_t rpiReturnI2Cresult(u_int8_t result);

// GPIO
bool rpiGpioInit(void)
{
	return bcm2835_init();
}

bool rpiGpioClose(void)
{
	return bcm2835_close();
}

void rpiGpioSetMode(uint8_t pin, uint8_t mode)
{
	if (mode == RPI_GPIO_INPUT){
		bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_INPT);
		return;
	}
	bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_OUTP);
}

void rpiGpioWrite(uint8_t pin, uint8_t value)
{
	bcm2835_gpio_write(pin, value);
}

bool rpiGpioRead(uint8_t pin)
{
	return bcm2835_gpio_lev(pin);
}

void rpiGpioPullDown(uint8_t pin)
{
	bcm2835_gpio_set_pud(pin, BCM2835_GPIO_PUD_DOWN);
}

void rpiDelay(uint32_t ms)
{
	bcm2835_delay(ms);
}

// I2C
bool rpiI2Cinit(void)
{
	return bcm2835_i2c_begin();
}

void rpiI2CsetTargetAddress(u_int8_t address)
{
	bcm2835_i2c_setSlaveAddress(address);
}

void rpiI2CsetBaudrate(uint32_t baudrate)
{
	bcm2835_i2c_set_baudrate(baudrate);
}

u_int8_t rpiI2Cwrite(char const *buffer, uint32_t len)
{
	uint8_t result = bcm2835_i2c_write(buffer, len);
	return rpiReturnI2Cresult(result);
}

u_int8_t rpiI2Cread(char *buffer, uint32_t len)
{
	uint8_t result = bcm2835_i2c_read(buffer, len);
	return rpiReturnI2Cresult(result);
}

void rpiI2Cclose(void)
{
	return bcm2835_i2c_end();
}

u_int8_t rpiReturnI2Cresult(u_int8_t result)
{
	if (result == BCM2835_I2C_REASON_OK)
	{
		return RPI_I2C_REASON_OK;
	} else if (result == BCM2835_I2C_REASON_ERROR_NACK)
	{
		return RPI_I2C_REASON_ERROR_NACK;
	} else if (result == BCM2835_I2C_REASON_ERROR_CLKT)
	{
		return RPI_I2C_REASON_ERROR_CLKT;
	} else if (result == BCM2835_I2C_REASON_ERROR_DATA)
	{
		return RPI_I2C_REASON_ERROR_DATA;
	#ifdef BCM2835_I2C_REASON_ERROR_TIMEOUT
	} else if (result == BCM2835_I2C_REASON_ERROR_TIMEOUT)
	{
		return RPI_I2C_REASON_ERROR_TIMEOUT;
	}
	#else
	} else {
		return RPI_I2C_REASON_ERROR_TIMEOUT;
	}
	#endif
}

// SPI
bool rpiSpiOpen(void)
{
	return bcm2835_spi_begin();
}
void rpiSpiClose(void)
{
	return bcm2835_spi_end();
}

void rpiSpiSetBitOrder(uint8_t order)
{
	if (order == RPI_SPI_LSBFIRST)
	{
		bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_LSBFIRST);
		return;
	}
	bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);
}

void rpiSpiSetMode(uint8_t mode)
{
	if (mode == RPI_SPI_MODE0)
	{
		bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);
	} else if (mode == RPI_SPI_MODE1) {
		bcm2835_spi_setDataMode(BCM2835_SPI_MODE1);
	} else if (mode == RPI_SPI_MODE2) {
		bcm2835_spi_setDataMode(BCM2835_SPI_MODE2);
	} else {
		bcm2835_spi_setDataMode(BCM2835_SPI_MODE3);
	}
}

void rpiSpiSetFrequency(uint16_t divider) // argument is clock divder that sets frequency
{
	if (divider == RPI_SPI_CLOCK_DIVIDER_128){
		bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_128);
	} else {
		bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_512);
	}
}
void rpiSpiSetChipSelect(uint8_t cs){
	if (cs == RPI_SPI_CS0)
	{
		bcm2835_spi_chipSelect(BCM2835_SPI_CS0);
		return;
	}
	bcm2835_spi_chipSelect(BCM2835_SPI_CS1);
}

void rpiSpiSetChipSelectPolarity(uint8_t cs, uint8_t active)
{
	if (cs == RPI_SPI_CS0)
	{
		bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, active);
		return;
	}
	bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS1, active);
}

uint8_t rpiSpiTransfer(uint8_t data)
{
	return bcm2835_spi_transfer(data);
}

void rpiSpiTransferN(uint8_t *buffer){
	bcm2835_spi_transfern((char *)buffer, sizeof(buffer));
}

// PWM
void rpiPWMsetValue(uint8_t channel, uint32_t value)
{
	bcm2835_pwm_set_data(channel, value);
}

void rpiPWMenableGPIO12(void)
{
	bcm2835_gpio_fsel(12, BCM2835_GPIO_FSEL_ALT0);
}

void rpiPWMsetClock(uint16_t clock_divider)
{
	if (clock_divider == RPI_PWM_CLOCK_DIVIDER_32)
	{
		bcm2835_pwm_set_clock(BCM2835_PWM_CLOCK_DIVIDER_32);
		return;
	}
	bcm2835_pwm_set_clock(BCM2835_PWM_CLOCK_DIVIDER_16);
}

void rpiPWMsetMode(uint8_t channel, uint8_t useMarkspace, uint8_t enabled)
{
	bcm2835_pwm_set_mode(channel, useMarkspace, enabled);
}

void rpiPWMsetRange(uint8_t channel, uint32_t range)
{
	bcm2835_pwm_set_range(channel, range);
}
