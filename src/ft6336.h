/*
 * Original author: Atsushi Sasaki(https://github.com/aselectroworks) 
*/

#ifndef _FT6336U_H
#define _FT6336U_H

#include <stdint.h>
#include <stdbool.h>

void ft6336Begin(void);
bool ft6336IsTouched(void);
bool ft6336ReadTouch1X(uint16_t *pData);
bool ft6336ReadTouch1Y(uint16_t *pData);
#endif
