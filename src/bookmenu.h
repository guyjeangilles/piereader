#ifndef _ER_BOOKMENU_H
#define _ER_BOOKMENU_H

void makeBookMenu(void);
void getBookpath(char *buffer);
void unfreezeBookpath(void);

void check_book_catalog(void);

#endif //_ER_BOOKMENU_H
