#include "lvgl/lvgl.h"
#include "gpio.h"
#include "logger.h"

#define RIGHT_BUTTON_PIN 23
#define LEFT_BUTTON_PIN 5
#define UP_BUTTON_PIN 27
#define DOWN_BUTTON_PIN 22
#define SELECT_BUTTON_PIN 26

static void lvKeypadRead(lv_indev_drv_t * indev_drv, lv_indev_data_t * data);
void keypadInit(void);

lv_indev_t * indev_keypad;

void lvKeypadSetup(void)
{
    // Input devices
    static lv_indev_drv_t indev_drv;
    keypadInit();

    // Register a keypad input device
    lv_indev_drv_init(&indev_drv);
    indev_drv.type = LV_INDEV_TYPE_KEYPAD;
    indev_drv.read_cb = lvKeypadRead;
    indev_keypad = lv_indev_drv_register(&indev_drv);

    // Create LVGL group for keypad
    lv_group_t *g = lv_group_create();
    lv_group_set_default(g);
    lv_indev_set_group(indev_keypad, g);
}

// call back when a key is pressed by lvgl
static void lvKeypadRead(lv_indev_drv_t * indev_drv, lv_indev_data_t * data)
{
    (void)indev_drv;

    static uint32_t lastKey = 0;
    bool pressed = false;

    if (gpioRead(RIGHT_BUTTON_PIN))
    {
	logDebug("Right button pressed (GPIO %u)", RIGHT_BUTTON_PIN);
	lastKey = LV_KEY_RIGHT;
	pressed = true;
    } else if (gpioRead(LEFT_BUTTON_PIN)) {
	logDebug("Left button pressed (GPIO %u)", LEFT_BUTTON_PIN);
	lastKey = LV_KEY_LEFT;
	pressed = true;
    } else if (gpioRead(UP_BUTTON_PIN)) {
	logDebug("Up button pressed (GPIO %u)", UP_BUTTON_PIN);
	lastKey = LV_KEY_PREV;
	pressed = true;
    } else if (gpioRead(DOWN_BUTTON_PIN)) {
	logDebug("Down button pressed (GPIO %u)", DOWN_BUTTON_PIN);
	lastKey = LV_KEY_NEXT;
	pressed = true;
    } else if (gpioRead(SELECT_BUTTON_PIN)) {
	logDebug("Select button pressed (GPIO %u)", SELECT_BUTTON_PIN);
	lastKey = LV_KEY_ENTER;
	pressed = true;
    }

    if (pressed) {
	    data->state = LV_INDEV_STATE_PR;
    } else {
	    data->state = LV_INDEV_STATE_REL;
    }
    data->key = lastKey;
}

void keypadInit(void)
{
    gpioSetMode(RIGHT_BUTTON_PIN, GPIO_INPUT);
    gpioSetMode(LEFT_BUTTON_PIN, GPIO_INPUT);
    gpioSetMode(UP_BUTTON_PIN, GPIO_INPUT);
    gpioSetMode(DOWN_BUTTON_PIN, GPIO_INPUT);
    gpioSetMode(SELECT_BUTTON_PIN, GPIO_INPUT);

    gpioPullDown(RIGHT_BUTTON_PIN);
    gpioPullDown(LEFT_BUTTON_PIN);
    gpioPullDown(UP_BUTTON_PIN);
    gpioPullDown(DOWN_BUTTON_PIN);
    gpioPullDown(SELECT_BUTTON_PIN);
}

lv_group_t *lvGetKeypadGroup(void)
{
    return lv_group_get_default();
}
