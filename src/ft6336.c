/*
 * Original author: Atsushi Sasaki(https://github.com/aselectroworks) 
*/
#include <stdbool.h>
#include "ft6336.h"
#include "gpio.h"
#include "logger.h"
#include "i2c.h"

#define I2C_ADDR_FT6336U 0x38

#define FT6336U_ADDR_TOUCH1_X 0x03
#define FT6336U_ADDR_TOUCH1_Y 0x05

#define RST_PIN 6
#define INT_PIN 16

bool ft6336ReadByte(uint8_t addr, uint8_t *pData);

void ft6336Begin(void) {
    // Initialize I2C
    logDebug("Initializing FT6336");
    gpioSetMode(INT_PIN, GPIO_INPUT);
    gpioSetMode(RST_PIN, GPIO_OUTPUT);
    gpioWrite(RST_PIN, 0);
    delayMs(10);
    gpioWrite(RST_PIN, 1);
    delayMs(500);
}

bool ft6336IsTouched(void)
{
    return gpioRead(INT_PIN) == 0;
}

bool ft6336ReadTouch1X(uint16_t *pData) {
    uint8_t buffer[2];
    if (ft6336ReadByte(FT6336U_ADDR_TOUCH1_X, &buffer[0]) != true)
    {
        return false;
    }
    if (ft6336ReadByte(FT6336U_ADDR_TOUCH1_X + 1, &buffer[1]) != true)
    {
        return false;
    }
    *pData = ((buffer[0] & 0x0f) << 8) | buffer[1];
    return true;
}

bool ft6336ReadTouch1Y(uint16_t *pData) {
    uint8_t buffer[2];
    if (ft6336ReadByte(FT6336U_ADDR_TOUCH1_Y, &buffer[0]) != true)
    {
        return false;
    }
    if (ft6336ReadByte(FT6336U_ADDR_TOUCH1_Y + 1, &buffer[1]) != true)
    {
        return false;
    }
    *pData = ((buffer[0] & 0x0f) << 8) | buffer[1];
    return true;
}

bool ft6336ReadByte(uint8_t addr, uint8_t *pData) {
    bool returnValue = true;

    i2cInit();
    i2cSetTargetAddress(I2C_ADDR_FT6336U);
    i2cSetBaudrate(100000);
    delayMs(10);
    uint8_t reason = i2cWrite((char *)&addr, sizeof(addr));
    if (reason != I2C_REASON_OK)
    {
        logError("Writing to FT6336 address %x failed with reason (%x)", addr, reason);
        returnValue = false;
        goto exit;
    }
    reason = i2cRead((char *)pData, sizeof(*pData));
    if (reason != I2C_REASON_OK)
    {
        logError("Reading from FT6336 address %x failed with reason (%x)", addr, reason);
        returnValue = false;
    }
    
    exit:
        i2cClose();
        return returnValue;
}
