#ifndef _PWM_H_
#define _PWM_H_
#include <stdint.h>

#define PWM_CLOCK_DIVIDER_32 32
#define PWM_CLOCK_DIVIDER_16 16

void pwmSetValue(uint8_t channel, uint32_t value);
void pwmEnable(void);
void pwmSetClock(uint16_t clock_divider);
void pwmSetRange(uint8_t channel, uint32_t range);

#endif //_PWM_H_
