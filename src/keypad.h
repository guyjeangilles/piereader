#ifndef _EREADER_KEYPAD_H_
#define _EREADER_KEYPAD_H_

#include "lvgl/lvgl.h"

void lvKeypadSetup(void);
lv_group_t *lvGetKeypadGroup(void);

#endif // _EREADER_KEYPAD_H_
