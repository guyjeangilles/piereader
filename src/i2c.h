#ifndef _I2C_H_
#define _I2C_H_

#include <stdbool.h>
#include <stdint.h>

#define I2C_REASON_OK 0x00
#define I2C_REASON_ERROR_NACK 0x01 	
#define I2C_REASON_ERROR_CLKT 0x02
#define I2C_REASON_ERROR_DATA 0x04
#define I2C_REASON_ERROR_TIMEOUT 0x08
#define I2C_ERROR_UNKNOWN 0xFF

bool i2cInit(void);
void i2cSetTargetAddress(uint8_t address);
void i2cSetBaudrate(uint32_t baudrate);
uint8_t i2cWrite(char const *buffer, uint32_t len);
uint8_t i2cRead(char *buffer, uint32_t len);
void i2cClose(void);


#endif //_I2C_H_
