#include <string.h>
#include <stddef.h>
#include <limits.h>
#include <stdlib.h>
#include <libgen.h>
#include <stdio.h>
#include "progression.h"
#include "document_renderer.h"
#include "filesystem.h"
#include "display.h"
#include "logger.h"

#define ROTATED_DISPLAY_X_RESOLUTION 300
#define ROTATED_DISPLAY_Y_RESOLUTION 400

#define LOCATOR_EXTENSION ".locator"
#define TEMP_EXTENSION ".tmp"
#define DEFAULT_STATE_SUBDIR_PATH ".local" DEFAULT_DIRECTORY_SEPARATOR "state" DEFAULT_DIRECTORY_SEPARATOR DEFAULT_SUB_DIR_NAME DEFAULT_DIRECTORY_SEPARATOR "progression" DEFAULT_DIRECTORY_SEPARATOR

#define LOCATOR_CONTENT_MAX 2048

#define LOCATOR_POSITION_KEY "{locations:{position:%d}}"
#define LOCATOR_TOTALPROGRESSION_KEY "{locations:{totalProgression:%f}}"
#define LOCATOR_TEMPLATE "\{\n\t\"href\": \"%s\",\n\t\"locations\": {\n\t\t\"position\": %u,\n\t\t\"totalProgression\": %f,\n\t}\n}"

pr_progression_t saveLocator(char *bookFilepath, uint32_t newPageNumber, uint32_t oldPageNumber);
void locatorFromBookFilepath(char *bookFilepath, char *buffer);
pr_progression_t writeTempLocator(char *tempFilepath, char *filepath, char *bookFilepath, uint32_t newPageNumber, uint32_t oldPageNumber);
pr_progression_t getPageCountFromLocator(uint32_t pageNumber, char *locatorFilepath, uint32_t *pageCount);

pr_progression_t saveLocator(char *bookFilepath, uint32_t newPageNumber, uint32_t oldPageNumber){
    char locatorFilepath[NAME_MAX];
    locatorFromBookFilepath(bookFilepath, locatorFilepath);

    char tempFilepath[NAME_MAX];
    pr_progression_t returnValue = writeTempLocator(tempFilepath, locatorFilepath, bookFilepath, newPageNumber, oldPageNumber);
    if (returnValue != STATUS_OK)
    {
        return returnValue;
    }

    if (rename(tempFilepath, locatorFilepath) != 0)
    {
        returnValue = FILE_RENAME_FAIL;
    }
    return returnValue;
}

void locatorFromBookFilepath(char *bookFilepath, char *buffer)
{
    char const *stateDir = getenv("XDG_STATE_HOME");
    
    if (stateDir == NULL || strlen(stateDir) == 0)
    { // No state directory environment variable, use default directory
	logDebug("State Directory is NULL or empty");
	char temp[NAME_MAX] = {0};
	getHomepath(temp);
	strcat(temp, DEFAULT_STATE_SUBDIR_PATH);
	strcpy(buffer, temp);
	logDebug("Partial locator path is %s", buffer);
    } else if (!endsWithSeparator(buffer)) {
	logDebug("Locator directory does not end with '/'");
	strcat(buffer, DEFAULT_DIRECTORY_SEPARATOR);
    }
    
    // get the book filename without the extension
    char *_basename = basename(bookFilepath);
    char *filenameEnd = strrchr(_basename, '.');
    if (filenameEnd == NULL)
    {
        strcat(buffer, _basename);
    } else {
        uint32_t str_len = (uint32_t)(filenameEnd - _basename + 1);
        strncat(buffer, _basename, str_len - 1);
    }
    strcat(buffer, LOCATOR_EXTENSION);
    logDebug("Locator path is %s", buffer);
}

pr_progression_t writeTempLocator(char *tempFilepath, char *filepath, char *bookFilepath, uint32_t newPageNumber, uint32_t oldPageNumber)
{
    if (filepath == NULL)
    {
        return FILE_WRITE_FAIL;
    }

    char _tempFilepath[NAME_MAX];
    strcpy(_tempFilepath, filepath);
    strcat(_tempFilepath, TEMP_EXTENSION);
    FILE *filedescriptor = fopen(_tempFilepath, "w");

    uint32_t pageCount = 0;
    float totalProgression;
    if (getPageCountFromLocator(oldPageNumber, filepath, &pageCount) != STATUS_OK)
    { // (slow) calculate page count by reflowing the book
        totalProgression = (float)newPageNumber / (float)getPageCount(bookFilepath, ROTATED_DISPLAY_X_RESOLUTION, ROTATED_DISPLAY_Y_RESOLUTION);
    } else {
        totalProgression = (float)newPageNumber / (float)pageCount;
    }
    logDebug("Calculated progression is %f", (double)totalProgression);

    if (fprintf(filedescriptor, LOCATOR_TEMPLATE, bookFilepath, newPageNumber, (double)totalProgression) <0)
    {
        return FILE_WRITE_FAIL;
    }
    strcpy(tempFilepath, _tempFilepath);
    fclose(filedescriptor);
    return STATUS_OK;
}

uint32_t getPageNumber(char *bookFilepath)
{
    char locatorFilepath[NAME_MAX];
    locatorFromBookFilepath(bookFilepath, locatorFilepath);

    char content[LOCATOR_CONTENT_MAX];
    er_filesystem_t returnValue = readJson(locatorFilepath, content);
    if (returnValue != FILE_PARSE_OK)
    {
        logWarning("Error parsing %s. File contents are %s", locatorFilepath, content);
        return 0;
    }

    u_int32_t pageNumber = 0;
    er_filesystem_t searchResult = searchJsonUInt(content, LOCATOR_POSITION_KEY, &pageNumber);
    if (searchResult != FILE_PARSE_OK)
    {
	logError("Error reading page number from locator file (%d)", searchResult);
	return 0;
    }
    return pageNumber;
}

pr_progression_t getPageCountFromLocator(uint32_t pageNumber, char *locatorFilepath, uint32_t *pageCount)
{
    char content[LOCATOR_CONTENT_MAX];
    er_filesystem_t returnValue = readJson(locatorFilepath, content);
    if (returnValue != FILE_PARSE_OK)
    {
        logWarning("Could not read  %s. File contents are %s", locatorFilepath, content);
        return FILE_READ_FAIL;
    }

    float totalProgression = 0.0;
    er_filesystem_t searchResult = searchJsonFloat(content, LOCATOR_TOTALPROGRESSION_KEY, &totalProgression);
    if (searchResult != FILE_PARSE_OK)
    {
        logError("Could not get page count from locator. Totalprogression key not found (%d)", searchResult);
        return FILE_READ_FAIL;
    }
    logDebug("Fetched progression is %f", (double)totalProgression);

    if (totalProgression == 0.0f)
    {
        logWarning("Not calculating page count because total progession is 0.");
        return FILE_DATA_FAIL;
    }
    *pageCount = (uint32_t)((float)pageNumber / totalProgression);
    logDebug("PageCount is %u", *pageCount);
    return STATUS_OK;
}

// return wheter the number changed
bool increasePageNumber(char *bookFilepath)
{
    uint32_t oldPageNumber = getPageNumber(bookFilepath);
    uint32_t newPageNumber = oldPageNumber + 1;

    char locatorFilepath[NAME_MAX];
    locatorFromBookFilepath(bookFilepath, locatorFilepath);
    uint32_t pageCount = 0;
    if (getPageCountFromLocator(oldPageNumber, locatorFilepath, &pageCount) != STATUS_OK)
    { // (slow) calculate page count by reflowing the book
        logDebug("Could not fetch page count from locator. Calculating it with document renderer.");
        pageCount = getPageCount(bookFilepath, ROTATED_DISPLAY_X_RESOLUTION, ROTATED_DISPLAY_Y_RESOLUTION);
    }

    if (newPageNumber > pageCount)
    {
        logDebug("Page number is greater than page count. Not changing");
        return false;
    }
    saveLocator(bookFilepath, newPageNumber, oldPageNumber);
    logDebug("Page number is now %u", newPageNumber);
    return true;
}

//return wheter the number changed
bool decreasePageNumber(char *bookFilepath)
{
    uint32_t pageNumber = getPageNumber(bookFilepath);
    if (pageNumber == 0)
    {
	    logDebug("Page number is zero. Not decreasing.");
	    return false;
    }
    pageNumber--;
    saveLocator(bookFilepath, pageNumber, pageNumber + 1);
    logDebug("Page number is now %u", pageNumber);
    return true;
}
