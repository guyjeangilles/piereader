"""Perform Bluetooth Related Actions."""
import argparse
import bluetooth
import sys


def discover(filename: str):
    for devices in bluetooth.discover_devices(lookup_names=True):
        with open(filename, "w") as fout:
            fout.write(";".join([devices[1], devices[0]]))  # put the name first


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        help="File where bluetooth devices will be saved.",
    )

    args = parser.parse_args()

    if not args.output:
        print("Please specify out.")
        sys.exit(1)

    discover(args.output)
