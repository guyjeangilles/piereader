#include "lvgl/lvgl.h"
#include "tps61165.h"
#include "gpio.h"
#include "logger.h"

#define BRIGHTEN_PIN 27
#define DIM_PIN 22
#define MAX_BRIGHTNESS 10

void backlightInit(void);
void brightenBacklight(void);
void dimBacklight(void);

uint8_t currentBrightness = 0;
bool lastBrightenPress = 0;
bool lastDimPress = 0;

static void lvBacklightKeypadRead(lv_indev_drv_t * indev_drv, lv_indev_data_t * data);
lv_indev_t * indevBacklightKeypad;
lv_group_t *backlightGroup;

// call back when a key is pressed by lvgl
static void lvBacklightKeypadRead(lv_indev_drv_t * indev_drv, lv_indev_data_t * data)
{
    (void)indev_drv;

    bool pressed = false;
    bool brightenButtonState = gpioRead(BRIGHTEN_PIN);
    bool dimButtonState = gpioRead(DIM_PIN);
	if (brightenButtonState == 1 && lastBrightenPress == 0)
	{
		logDebug("Backlight brighten button pressed (GPIO %u)", BRIGHTEN_PIN);
		brightenBacklight();
		pressed = true;
	} else if (dimButtonState == 1 && lastDimPress == 0) {
		logDebug("Backlight dim button pressed (GPIO %u)", DIM_PIN);
		dimBacklight();
		pressed = true;
	}
	lastBrightenPress = brightenButtonState;
	lastDimPress = dimButtonState;

    if (pressed) {
	    data->state = LV_INDEV_STATE_PR;
    } else {
	    data->state = LV_INDEV_STATE_REL;
    }
}

void lvKBacklightKeypadSetup(void)
{
    // Initializing backlight
    backlightInit();

    // Register a keypad input device
    static lv_indev_drv_t indevDriver;
    lv_indev_drv_init(&indevDriver);
    indevDriver.type = LV_INDEV_TYPE_KEYPAD;
    indevDriver.read_cb = lvBacklightKeypadRead;
    indevBacklightKeypad = lv_indev_drv_register(&indevDriver);

    // Set LVGL group for keypad
    lv_indev_set_group(indevBacklightKeypad, backlightGroup);
}

void backlightInit(void)
{
	tps61165Init();
	
    gpioSetMode(BRIGHTEN_PIN, GPIO_INPUT);
    gpioSetMode(DIM_PIN, GPIO_INPUT);

    gpioPullDown(BRIGHTEN_PIN);
    gpioPullDown(DIM_PIN);
}

void brightenBacklight(void)
{
	if (currentBrightness >= MAX_BRIGHTNESS)
	{
		logInfo("Not brightenning backlight beyond max brightness");
		return;
	}
	currentBrightness++;
	tps61165SetBrightness(currentBrightness);
}

void dimBacklight(void)
{
	if (currentBrightness <= 0)
	{
		logInfo("Backlight is off, not dimming");
		return;
	}
	currentBrightness--;
	tps61165SetBrightness(currentBrightness);
}

lv_group_t *lvGetBacklightGroup(void)
{
    return backlightGroup;
}

