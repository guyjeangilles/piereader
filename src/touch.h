#ifndef _EREADER_TOUCH_H_
#define _EREADER_TOUCH_H_

#include "lvgl/lvgl.h"

void lvTouchSetup(void);
lv_group_t *lvGetTouchGroup(void);

#endif // _EREADER_TOUCH_H_
