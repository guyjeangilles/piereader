#include "gpio.h"
#include "spi.h"
#include "gdey042t81.h"
#include "logger.h"

#define	BUSY_Pin 24
#define	RES_Pin 17
#define	DC_Pin 25
#define	CS_Pin  8

#define MAX_LINE_BYTES 50// =400/8
#define MAX_COLUMN_BYTES  300

#define ALLSCREEN_GRAGHBYTES  15000

void gdey042t81DeepSleep(void);
void gdey042t81ReadBusy(void);

void gdey042t81DisplayFast(uint8_t const *imageBuffer);                        
void gdey042t81InitFast(void); 
void gdey042t81ClearFast(void);
void gdey042t81UpdateFast(void);

void gdey042t81SafeClose(void)
{
  delayMs(2000);  
  gdey042t81InitFast();
  gdey042t81ClearFast();
  gdey042t81DeepSleep();
}

bool gdey042t81Setup()
{
   gpioSetMode(BUSY_Pin, GPIO_INPUT);
   gpioSetMode(RES_Pin, GPIO_OUTPUT);
   gpioSetMode(DC_Pin, GPIO_OUTPUT);
   gpioSetMode(CS_Pin, GPIO_OUTPUT);
   
   if(!spiOpen())
   {
     logError("Failed to open SPI protocol.");
     return false;
   }
   spiSetBitOrder(SPI_MSBFIRST);
   spiSetMode(SPI_MODE0);
   spiSetFrequency(SPI_CLOCK_DIVIDER_128);
   return true;
}

void gdey042t81WriteCommand(uint8_t command)
{
  gpioWrite(CS_Pin, 0);                   
  gpioWrite(DC_Pin, 0);   // command write
  spiTransfer(command);
  gpioWrite(CS_Pin, 1);
}
void gdey042t81WriteData(uint8_t data)
{
  gpioWrite(CS_Pin, 0);                   
  gpioWrite(DC_Pin, 1);   // data write
  spiTransfer(data);
  gpioWrite(CS_Pin, 1);
}

void gdey042t81InitFast(void)
{
  gpioWrite(RES_Pin, 0);  // Module reset
  delayMs(10); // At ;east 10ms delay
  gpioWrite(RES_Pin, 1);; 
  delayMs(10); // At ;east 10ms delay
  
  gdey042t81WriteCommand(0x12);  //SWRESET
  gdey042t81ReadBusy();   
 
  gdey042t81WriteCommand(0x21);  
  gdey042t81WriteData(0x40);
  gdey042t81WriteData(0x00); 
  
  gdey042t81WriteCommand(0x3C);     
  gdey042t81WriteData(0x05);  

  //1.0s
  gdey042t81WriteCommand(0x1A); // Write to temperature register
  gdey042t81WriteData(0x5A);      
            
  gdey042t81WriteCommand(0x22); // Load temperature value
  gdey042t81WriteData(0x91);    
  gdey042t81WriteCommand(0x20); 
  gdey042t81ReadBusy();   
  
  gdey042t81WriteCommand(0x11);  // Data entry mode
  gdey042t81WriteData(0x01);   
  
  gdey042t81WriteCommand(0x44); 
  gdey042t81WriteData(0x00); // RAM x address start at 0
  gdey042t81WriteData(0x31); // RAM x address end at 31h(49+1)*8->400
  gdey042t81WriteCommand(0x45); 
  gdey042t81WriteData(0x2B);   // RAM y address start at 12Bh     
  gdey042t81WriteData(0x01);
  gdey042t81WriteData(0x00); // RAM y address end at 00h     
  gdey042t81WriteData(0x00);
  
  gdey042t81WriteCommand(0x4E); 
  gdey042t81WriteData(0x00);
  gdey042t81WriteCommand(0x4F); 
  gdey042t81WriteData(0x2B);
  gdey042t81WriteData(0x01);  
}

void gdey042t81UpdateFast(void)
{   
  gdey042t81WriteCommand(0x22); //Display Update Control
  gdey042t81WriteData(0xC7);   
  gdey042t81WriteCommand(0x20); //Activate Display Update Sequence
  gdey042t81ReadBusy();   
}

void gdey042t81PartialUpdate(void)
{
  gdey042t81WriteCommand(0x22); //Display Update Control
  gdey042t81WriteData(0xFF);   
  gdey042t81WriteCommand(0x20); //Activate Display Update Sequence
  gdey042t81ReadBusy();      
}

void gdey042t81DeepSleep(void)
{  
 gdey042t81WriteCommand(0x10); //enter deep sleep
 gdey042t81WriteData(0x01); 
  delayMs(100);
}

void gdey042t81ReadBusy(void)
{ 
  while(1)
  {   //=1 BUSY
     if(gpioRead(BUSY_Pin)==0) break;
  }  
}

void gdey042t81ClearFast(void)
{
   unsigned int i;  
  gdey042t81WriteCommand(0x24);   //write RAM for black(0)/white (1)
   for(i=0;i<ALLSCREEN_GRAGHBYTES;i++)
   {               
     gdey042t81WriteData(0xFF);
   }
  gdey042t81WriteCommand(0x26);   //write RAM for black(0)/white (1)
   for(i=0;i<ALLSCREEN_GRAGHBYTES;i++)
   {               
     gdey042t81WriteData(0xFF);
   }

  gdey042t81UpdateFast(); 
}

void gdey042t81DisplayFast(uint8_t const *imageBuffer)
{
  unsigned int i;     
  gdey042t81WriteCommand(0x24);   //Write Black and White image to RAM
   for(i=0;i<15000;i++)
   {               
     gdey042t81WriteData(imageBuffer[i]);
   }
  gdey042t81WriteCommand(0x26);   //Write Black and White image to RAM
   for(i=0;i<15000;i++)
   {               
     gdey042t81WriteData(imageBuffer[i]);
   }  
   gdey042t81UpdateFast(); 
}

void gdey042t81PartialDisplay(uint8_t const *imageBuffer)
{
  gpioWrite(RES_Pin, 0);  // Module reset
  delayMs(10); // At ;east 10ms delay
  gpioWrite(RES_Pin, 1);
  delayMs(10); // At least 10ms delay

  gdey042t81WriteCommand(0x3C); // Border Waveform
  gdey042t81WriteData(0x80);

  gdey042t81WriteCommand(0x21);
  gdey042t81WriteData(0x00);
  gdey042t81WriteData(0x00);

  gdey042t81WriteCommand(0x24);
  for (uint16_t i; i < MAX_COLUMN_BYTES * MAX_LINE_BYTES; i++)
  {
    gdey042t81WriteData(imageBuffer[i]);
  }

  gdey042t81PartialUpdate();
}
