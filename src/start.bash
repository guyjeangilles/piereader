# TODO make these absolute paths
export ER_DOWNLOAD_BOOKS_EXEC="venv/bin/python src/calibre_fetcher.py -d &"
export ER_CHECK_AVAILABLE_BOOKS_EXEC="venv/bin/python src/calibre_fetcher.py -c"
export ER_ADD_BOOKSERVER_TEMPLATE_EXEC="venv/bin/python src/calibre_fetcher.py -a "
export ER_BT_DISCOVER_TEMPLATE_EXEC="venv/bin/python src/bluetooth_discover.py -o "

./build/bin/main
