#include "input.h"

#ifdef TOUCH
	#include "touch.h"
#else
	#include "keypad.h"
#endif

void lvInputSetup(void)
{
	#ifdef TOUCH
		lvTouchSetup();
	#else
		lvKeypadSetup();
	#endif
}

lv_group_t *lvGetInputGroup(void)
{
	#ifdef TOUCH
		lvGetTouchGroup();
	#else
		lvGetKeypadGroup();
	#endif
}
