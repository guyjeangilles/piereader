#ifndef _ER_DOCUMENT_RENDERER_H_
#define _ER_DOCUMENT_RENDERER_H_

enum Render_Status {
  RENDER_OK = 0,
  RENDER_INIT_FAIL = -1,
  RENDER_OPEN_FAIL = -2,
  RENDER_PAGE_FAIL = -3,
  RENDER_RESOLUTION_FAIL = -4,
  RENDER_HANDLERS_FAIL = -5,
  RENDER_REFLOW_FAIL = -6,
  RENDER_SCALE_FAIL = -7,
  RENDER_RESIZE_FAIL = -8,
};

typedef int8_t dr_render_status_t;

uint32_t getPageCount(char *filepath, float xRes, float yRes);
dr_render_status_t fillImage(char *filepath, float xRes, float yRes, uint32_t pageNumber, uint8_t *imageBuffer);

#endif //_ER_DOCUMENT_RENDERER_H_
