#include <limits.h>
#include "lvgl/lvgl.h"
#include "bookviewer.h"
#include "document_renderer.h"
#include "bookmenu.h"
#include "display.h"
#include "input.h"
#include "progression.h"
#include "logger.h"

#define RETURN_ZONE_WIDTH LV_HOR_RES
#define RETURN_ZONE_HEIGHT (LV_VER_RES / 10)
#define RETURN_ZONE_LEFT 0
#define RETURN_ZONE_TOP (LV_VER_RES - RETURN_ZONE_HEIGHT)

#define PREV_PAGE_ZONE_WIDTH LV_HOR_RES / 3
#define PREV_PAGE_ZONE_HEIGHT LV_VER_RES - RETURN_ZONE_HEIGHT
#define PREV_PAGE_ZONE_LEFT 0
#define PREV_PAGE_ZONE_TOP 0

#define NEXT_PAGE_ZONE_WIDTH  LV_HOR_RES - PREV_PAGE_ZONE_WIDTH
#define NEXT_PAGE_ZONE_HEIGHT LV_VER_RES - RETURN_ZONE_HEIGHT
#define NEXT_PAGE_ZONE_LEFT PREV_PAGE_ZONE_LEFT + PREV_PAGE_ZONE_WIDTH
#define NEXT_PAGE_ZONE_TOP 0

static void returnHomeEventCb(lv_event_t * e);
static void lvMakeBookImage(lv_event_t * e);
#ifdef TOUCH
    static void increasePageCb(lv_event_t * e);
    static void decreasePageCb(lv_event_t * e);
#else
    static void keyEventCb(lv_event_t * e);
#endif

uint8_t imageData[DISPLAY_X_RESOLUTION*DISPLAY_Y_RESOLUTION];

void makeBookImage(void)
{
    char filepath[NAME_MAX];
    getBookpath(filepath);
    dr_render_status_t result = fillImage(filepath, ROTATED_DISPLAY_X_RESOLUTION, ROTATED_DISPLAY_Y_RESOLUTION, getPageNumber(filepath), imageData);
    if (result != RENDER_OK)
    {
        logWarning("Did not properly render book image.");
    }

    static lv_img_dsc_t imageDescription = {
        .header.always_zero = 0,
        .header.w = ROTATED_DISPLAY_X_RESOLUTION,
        .header.h = ROTATED_DISPLAY_Y_RESOLUTION,
        .data_size = ROTATED_DISPLAY_X_RESOLUTION * ROTATED_DISPLAY_Y_RESOLUTION * LV_COLOR_DEPTH / 8,
        .header.cf = LV_IMG_CF_ALPHA_8BIT,
        .data = imageData,
    };

    lv_obj_t *image = lv_img_create(lv_scr_act());
    lv_img_set_src(image, &imageDescription);
    #ifdef TOUCH
        static bool firstView = true;
        static lv_style_t containerStyle;
        lv_style_init(&containerStyle);
        static void (*pReturnHomeContainerCb)(lv_event_t * e);
        static void (*pPrevPageContainerCb)(lv_event_t * e);
        static void (*pNextPageContainerCb)(lv_event_t * e);

        if (firstView)
        {
            // Show the regions of the screen that will flip to next page,
            // flip to previous page, and return to book menu
            lv_style_set_bg_color(&containerStyle, lv_color_black());

            pReturnHomeContainerCb = lvMakeBookImage;
            pPrevPageContainerCb = lvMakeBookImage;
            pNextPageContainerCb = lvMakeBookImage;
        } else {
            // Make the touch regions invisible
            lv_style_set_bg_opa(&containerStyle, LV_OPA_TRANSP);

            pReturnHomeContainerCb = returnHomeEventCb;
            pPrevPageContainerCb = decreasePageCb;
            pNextPageContainerCb = increasePageCb;
        }

        lv_obj_t * returnHomeContainer = lv_obj_create(lv_scr_act());
        lv_obj_add_style(returnHomeContainer, &containerStyle, 0);
        lv_obj_set_pos(returnHomeContainer, RETURN_ZONE_LEFT, RETURN_ZONE_TOP);
        lv_obj_set_size(returnHomeContainer, RETURN_ZONE_WIDTH, RETURN_ZONE_HEIGHT);
        lv_obj_add_flag(returnHomeContainer, LV_OBJ_FLAG_CLICKABLE);
        lv_obj_add_event_cb(returnHomeContainer, pReturnHomeContainerCb, LV_EVENT_PRESSED, NULL);

        lv_obj_t * previousPageContainer = lv_obj_create(lv_scr_act());
        lv_obj_add_style(previousPageContainer, &containerStyle, 0);
        lv_obj_set_pos(previousPageContainer, PREV_PAGE_ZONE_LEFT, PREV_PAGE_ZONE_TOP);
        lv_obj_set_size(previousPageContainer, PREV_PAGE_ZONE_WIDTH, PREV_PAGE_ZONE_HEIGHT);
        lv_obj_add_flag(previousPageContainer, LV_OBJ_FLAG_CLICKABLE);
        lv_obj_add_event_cb(previousPageContainer, pPrevPageContainerCb, LV_EVENT_PRESSED, NULL);

        lv_obj_t * nextPageContainer = lv_obj_create(lv_scr_act());
        lv_obj_add_style(nextPageContainer, &containerStyle, 0);
        lv_obj_set_pos(nextPageContainer, NEXT_PAGE_ZONE_LEFT, NEXT_PAGE_ZONE_TOP);
        lv_obj_set_size(nextPageContainer, NEXT_PAGE_ZONE_WIDTH, NEXT_PAGE_ZONE_HEIGHT);
        lv_obj_add_flag(nextPageContainer, LV_OBJ_FLAG_CLICKABLE);
        lv_obj_add_event_cb(nextPageContainer, pNextPageContainerCb, LV_EVENT_PRESSED, NULL);

        if (firstView)
        {
            // Label touch regions
            firstView = false;
            static lv_style_t whiteTextStyle;
            lv_style_init(&whiteTextStyle);
            lv_style_set_text_color(&whiteTextStyle, lv_color_white());

            lv_obj_t *returnHomelabel = lv_label_create(returnHomeContainer);
            lv_label_set_text(returnHomelabel, "Return To Book Menu");
            lv_label_set_long_mode(returnHomelabel, LV_LABEL_LONG_WRAP);
            lv_obj_set_align(returnHomelabel, LV_ALIGN_CENTER);
            lv_obj_add_style(returnHomelabel, &whiteTextStyle, 0);
            lv_obj_set_style_text_align(returnHomelabel, LV_TEXT_ALIGN_CENTER, 0);

            lv_obj_t *prevPagelabel = lv_label_create(previousPageContainer);
            lv_label_set_text(prevPagelabel, "Previous Page");
            lv_label_set_long_mode(prevPagelabel, LV_LABEL_LONG_WRAP);
            lv_obj_set_align(prevPagelabel, LV_ALIGN_CENTER);
            lv_obj_add_style(prevPagelabel, &whiteTextStyle, 0);
            lv_obj_set_style_text_align(prevPagelabel, LV_TEXT_ALIGN_CENTER, 0);
            
            lv_obj_t *nextPagelabel = lv_label_create(nextPageContainer);
            lv_label_set_text(nextPagelabel, "Next Page");
            lv_label_set_long_mode(nextPagelabel, LV_LABEL_LONG_WRAP);
            lv_obj_set_align(nextPagelabel, LV_ALIGN_CENTER);
            lv_obj_add_style(nextPagelabel, &whiteTextStyle, 0);
            lv_obj_set_style_text_align(nextPagelabel, LV_TEXT_ALIGN_CENTER, 0);
        }
    #else
        lv_group_t *g = lvGetInputGroup();
        lv_group_add_obj(g, image);
        lv_obj_add_event_cb(image, keyEventCb, LV_EVENT_KEY, NULL);
        lv_obj_add_event_cb(image, returnHomeEventCb, LV_EVENT_PRESSED, NULL);
    #endif
}

static void returnHomeEventCb(lv_event_t * e)
{
    (void) e;
    logDebug("Pressed page event callback executed.");
    unfreezeBookpath();
    lv_obj_clean(lv_scr_act()); // delete everything on screen
    makeBookMenu();
}

static void lvMakeBookImage(lv_event_t * e)
{
    (void) e;
    makeBookImage();
}

#ifdef TOUCH
    static void increasePageCb(lv_event_t * e)
    {
        (void) e;
        logDebug("Increase page number callback executed.");

        char bookpath[NAME_MAX];
        getBookpath(bookpath);
        if (increasePageNumber(bookpath))
        {
            lv_obj_clean(lv_scr_act()); // delete everything on screen
            makeBookImage();
        }
    }

    static void decreasePageCb(lv_event_t * e)
    {
        (void) e;
        logDebug("Decrease page number callback executed.");

        char bookpath[NAME_MAX];
        getBookpath(bookpath);
        if (decreasePageNumber(bookpath))
        {
            lv_obj_clean(lv_scr_act()); // delete everything on screen
            makeBookImage();
        }
    }
#else
    static void keyEventCb(lv_event_t * e)
    {
        (void) e;
        logDebug("Key event callback executed.");
        bool changedPageNumber = 0;
        uint32_t key = lv_indev_get_key(lv_indev_get_act());

        char bookpath[NAME_MAX];
        getBookpath(bookpath);
        if (key == LV_KEY_RIGHT){
            changedPageNumber = increasePageNumber(bookpath);
        } else if (key == LV_KEY_LEFT){
            changedPageNumber = decreasePageNumber(bookpath);
        }

        if (changedPageNumber)
        {
            lv_obj_clean(lv_scr_act()); // delete everything on screen
            makeBookImage();
        }
    }
#endif
