#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include "logger.h"
#include "config_parser.h"
#include "filesystem.h"

#define DEFAULT_CONFIG_DIR_NAME ".config"
#define DEFAULT_CONFIG_PATH DEFAULT_HOME_PATH DEFAULT_CONFIG_DIR_NAME DEFAULT_DIRECTORY_SEPARATOR

#define CONFIG_SUB_DIR_NAME DEFAULT_SUB_DIR_NAME
#define CONFIG_FILE_NAME "config.json"
#define CONFIG_SUB_DIR_PATH CONFIG_SUB_DIR_NAME DEFAULT_DIRECTORY_SEPARATOR CONFIG_FILE_NAME

#define CONFIG_CONTENT_MAX_SIZE 1024

#define BOOK_DIRECTORY_FORMAT "{book_directory: %Q}"

void getConfigFilepath(char *buffer);
void getConfigDirectory(char *buffer); // TODO combine with config FilePath?

er_filesystem_t getBookDirectory(char *buffer)
{
	//read content from file
	char content[CONFIG_CONTENT_MAX_SIZE];
	char filepathBuffer[NAME_MAX];
	getConfigFilepath(filepathBuffer);
	er_filesystem_t result = readJson((char *)filepathBuffer, content);
	logDebug("JSON content is: %s", content);
	if (result != FILE_PARSE_OK)
	{
		return result;
	}

	// parse book directory from JSON content
	char *temp = 0;
	er_filesystem_t readResult = searchJsonStr(content, BOOK_DIRECTORY_FORMAT, &temp);
	if (readResult != FILE_PARSE_OK)
	{
		return readResult;
	}

	strcpy(buffer, temp);
	return FILE_PARSE_OK;
}

void getConfigFilepath(char *buffer)
{	
	getConfigDirectory(buffer);
	logDebug("Config filepath buffer is: %s", buffer);
	strcat(buffer, CONFIG_SUB_DIR_PATH);
	logDebug("Config filepath buffer is: %s", buffer);
}

void getConfigDirectory(char *buffer)
{
	char const *configDir = getenv("XDG_CONFIG_HOME");
	logDebug("Config directory environment variable: %s", configDir);
	if (configDir == NULL)
	{
		logDebug("Config Directory is NULL");
		char temp[NAME_MAX] = {0};
		getHomepath(temp);

		strcat(temp, DEFAULT_CONFIG_DIR_NAME);
		strcat(temp, DEFAULT_DIRECTORY_SEPARATOR);
		strcpy(buffer, temp);
		return;
	} else if (strlen(configDir) == 0) { // string is empty
		logDebug("Config directory is empty");
		strcpy((char *)configDir, DEFAULT_CONFIG_PATH);
		strcpy(buffer, configDir);
		return;
	} else if (!endsWithSeparator(configDir)) {
		logDebug("Config directory does not end with '/'");
		strcat((char *)configDir, "/");
		strcpy(buffer, configDir);
		return;
	}
	strcpy(buffer, configDir);
}
