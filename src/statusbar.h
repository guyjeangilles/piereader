#ifndef _ER_STATUSBAR_H_
#define _ER_STATUSBAR_H_

#include "lvgl/lvgl.h"

lv_obj_t* makeStatusbar(lv_obj_t *parent);

#endif // _ER_STATUSBAR_H_
