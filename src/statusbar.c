#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include "statusbar.h"
#include "lvgl/lvgl.h"
#include "logger.h"
#include "bookmenu.h"
#include "time.h"
#include "wifi.h"
#include "battery.h"
#include "bluetooth.h"

#define MENU_OPTION_NAME_MAX 256
#define SERVER_MENU_NAME "Add Book Server..."

#define APPLY_BUTTON_NAME "Apply"
#define CANCEL_BUTTON_NAME "Close"

static void dropdownListStyleCB(lv_event_t *e);
static void settingsListSelectionManager(lv_event_t *e);
static void serverFormCb(lv_event_t *e);

void makeBookServerForm(void);
bool storeBookserverURL(char const *serverURL);

void clockCB(lv_timer_t *timer);

lv_obj_t *bluetoothMenu;
void btMenuRefresh(void);
static void btMenuRefreshCB(lv_event_t *e);
static void btSelectionManagerCB(lv_event_t *e);

lv_obj_t *wifiMenu;
static void wifiSelectionManagerCB(lv_event_t *e);
static void wifirFormCb(lv_event_t *e);
static void wifiMenuRefreshCB(lv_event_t *e);
void wifiMenuRefresh(void);
void makeWifiForm(char *ssid);

struct wifiFormData {
	lv_obj_t *keyboard;
	char *ssid;
	char *password;
};
struct wifiFormData wfd;

lv_obj_t *batteryLabel;
void batteryCB(lv_timer_t *timer);
void batteryPercentRefresh(void);

lv_obj_t* makeStatusbar(lv_obj_t *parent)
{
    lv_obj_t *statusBar = lv_obj_create(parent);
    lv_obj_set_size(statusBar, lv_pct(100), 48);
    lv_obj_set_style_bg_color(statusBar, lv_color_white(), LV_PART_MAIN);
    lv_obj_set_style_radius(statusBar, 0, 0);
    lv_obj_clear_flag(statusBar, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_flex_flow(statusBar, LV_FLEX_FLOW_ROW);

    // settings menu
    lv_obj_t *settingsMenu = lv_dropdown_create(statusBar);
    lv_dropdown_set_text(settingsMenu, LV_SYMBOL_SETTINGS);
    lv_obj_set_style_text_align(lv_dropdown_get_list(settingsMenu), LV_TEXT_ALIGN_CENTER, 0);
    lv_dropdown_set_options(settingsMenu, SERVER_MENU_NAME);
    lv_dropdown_set_selected_highlight(settingsMenu, false);
    lv_dropdown_set_symbol(settingsMenu, NULL);

    lv_obj_set_size(settingsMenu, 48, lv_pct(100));
    lv_obj_set_align(settingsMenu, LV_ALIGN_LEFT_MID);
    lv_obj_set_style_text_color(settingsMenu, lv_color_black(), LV_PART_MAIN);
    lv_obj_set_style_bg_color(settingsMenu, lv_color_white(), LV_PART_MAIN);
    lv_obj_set_style_border_color(settingsMenu, lv_color_white(), LV_PART_MAIN);
    lv_obj_set_style_bg_color(settingsMenu, lv_color_black(), LV_PART_MAIN | LV_STATE_PRESSED);
    lv_obj_set_style_text_color(settingsMenu, lv_color_white(), LV_PART_MAIN | LV_STATE_PRESSED);

    lv_obj_add_event_cb(settingsMenu, dropdownListStyleCB, LV_EVENT_READY, NULL);
    lv_obj_add_event_cb(settingsMenu, settingsListSelectionManager, LV_EVENT_VALUE_CHANGED, NULL);

	// bluetooth
	bluetoothMenu = lv_dropdown_create(statusBar);
	lv_dropdown_set_text(bluetoothMenu, LV_SYMBOL_BLUETOOTH);

	btInit();
	btMenuRefresh();
    lv_obj_set_style_text_align(lv_dropdown_get_list(bluetoothMenu), LV_TEXT_ALIGN_CENTER, 0);
    lv_dropdown_set_selected_highlight(bluetoothMenu, false);
    lv_dropdown_set_symbol(bluetoothMenu, NULL);

    lv_obj_set_size(bluetoothMenu, 48, lv_pct(100));
    lv_obj_set_align(bluetoothMenu, LV_ALIGN_LEFT_MID);
    lv_obj_set_style_text_color(bluetoothMenu, lv_color_black(), LV_PART_MAIN);
    lv_obj_set_style_bg_color(bluetoothMenu, lv_color_white(), LV_PART_MAIN);
    lv_obj_set_style_border_color(bluetoothMenu, lv_color_white(), LV_PART_MAIN);
    lv_obj_set_style_bg_color(bluetoothMenu, lv_color_black(), LV_PART_MAIN | LV_STATE_PRESSED);
    lv_obj_set_style_text_color(bluetoothMenu, lv_color_white(), LV_PART_MAIN | LV_STATE_PRESSED);

	lv_obj_add_event_cb(bluetoothMenu, dropdownListStyleCB, LV_EVENT_READY, NULL);
    lv_obj_add_event_cb(bluetoothMenu, btSelectionManagerCB, LV_EVENT_VALUE_CHANGED, NULL);
    lv_obj_add_event_cb(bluetoothMenu, btMenuRefreshCB, LV_EVENT_REFRESH, NULL);

	// wifi
	wifiMenu = lv_dropdown_create(statusBar);
	wifiMenuRefresh();
    lv_dropdown_set_selected_highlight(wifiMenu, false);
    lv_dropdown_set_symbol(wifiMenu, NULL);

	lv_obj_set_size(wifiMenu, 48, lv_pct(100));
    lv_obj_set_align(wifiMenu, LV_ALIGN_LEFT_MID);
    lv_obj_set_style_text_color(wifiMenu, lv_color_black(), LV_PART_MAIN);
    lv_obj_set_style_bg_color(wifiMenu, lv_color_white(), LV_PART_MAIN);
    lv_obj_set_style_border_color(wifiMenu, lv_color_white(), LV_PART_MAIN);
    lv_obj_set_style_bg_color(wifiMenu, lv_color_black(), LV_PART_MAIN | LV_STATE_PRESSED);
    lv_obj_set_style_text_color(wifiMenu, lv_color_white(), LV_PART_MAIN | LV_STATE_PRESSED);

	lv_obj_add_event_cb(wifiMenu, dropdownListStyleCB, LV_EVENT_READY, NULL);
    lv_obj_add_event_cb(wifiMenu, wifiSelectionManagerCB, LV_EVENT_VALUE_CHANGED, NULL);
    lv_obj_add_event_cb(wifiMenu, wifiMenuRefreshCB, LV_EVENT_REFRESH, NULL);

	// clock
	lv_obj_t *clock = lv_label_create(statusBar);

	char timeStr[ER_CLOCK_BUF_SIZE];
	currentTime(timeStr);
	lv_label_set_text(clock, timeStr);
	lv_timer_create(clockCB, 60000, clock); // clock callback

    // battery
    batteryLabel = lv_label_create(statusBar);
	lv_obj_set_style_text_align(batteryLabel, LV_TEXT_ALIGN_CENTER, 0);
	lv_obj_set_style_text_color(batteryLabel, lv_color_black(), LV_PART_MAIN);

	batteryPercentRefresh();
	lv_timer_create(batteryCB, 60000, batteryLabel); // battery callback
	return statusBar;
}

static void dropdownListStyleCB(lv_event_t *e)
{
    lv_obj_t *settingsMenuList = lv_dropdown_get_list(lv_event_get_target(e));
    if (settingsMenuList == NULL)
    {
        logWarning("Could not fetch setting menu list after it was ready.");
    } else {
        // outline around setting list
        lv_obj_set_style_outline_width(settingsMenuList, 3, LV_PART_MAIN);
        lv_obj_set_style_outline_color(settingsMenuList, lv_color_black(), LV_PART_MAIN);

        // invert colors when pressed
        logDebug("Settings list exists after it was ready.");
        lv_obj_set_style_bg_color(settingsMenuList, lv_color_black(), LV_PART_MAIN | LV_STATE_PRESSED);
        lv_obj_set_style_bg_color(settingsMenuList, lv_color_black(), LV_PART_SELECTED | LV_STATE_PRESSED);
        lv_obj_set_style_text_color(settingsMenuList, lv_color_white(), LV_PART_SELECTED | LV_STATE_PRESSED);
    }
}

static void settingsListSelectionManager(lv_event_t *e)
{
    lv_obj_t *settingsMenu = lv_event_get_target(e);
    lv_obj_t *settingsMenuList = lv_dropdown_get_list(settingsMenu);
    if (settingsMenuList == NULL)
    {
        logWarning("Could not fetch setting menu list after value changed.");
    } else {
        logDebug("Settings list exists after value changed");
        char selectedBuffer[MENU_OPTION_NAME_MAX];
        lv_dropdown_get_selected_str(settingsMenu, selectedBuffer, MENU_OPTION_NAME_MAX);

        if (strcmp(selectedBuffer, SERVER_MENU_NAME) == 0)
        {
            logDebug("Selected Add Book Server...");
            makeBookServerForm();
        }
    }
}

static void btMenuRefreshCB(lv_event_t *e)
{
	(void) e;
	btMenuRefresh();
}


void btMenuRefresh(void)
{
	if(!btScan())
	{
		logError("Failed to scan bluetooth devices.");
		return;
	}

    u_int32_t length = btGetDeviceListSize();
    char btBuffer[length + 1];
    if(!btDevices(btBuffer, length + 1))
    {
		logError("Failed to read bluetooth devices.");
		return;
	}
	lv_dropdown_set_options(bluetoothMenu, btBuffer);
	btPurge();
}

static void btSelectionManagerCB(lv_event_t *e)
{
    lv_obj_t *_bluetoothMenu = lv_event_get_target(e);
    lv_obj_t *bluetoothMenuList = lv_dropdown_get_list(_bluetoothMenu);
    if (bluetoothMenuList == NULL)
    {
        logWarning("Could not fetch bluetooth menu list after value changed.");
    } else {
        logDebug("Bluetooth list exists after value changed");
        char selectedBuffer[MENU_OPTION_NAME_MAX];
        lv_dropdown_get_selected_str(_bluetoothMenu, selectedBuffer, MENU_OPTION_NAME_MAX);

        char *charPtr = strchr(selectedBuffer, ER_BLUETOOTH_DEVICES_DELIMITER);
        uint8_t position = 0;
        if (charPtr == NULL)
        {
			logWarning("Parsing bluetooth SSID did not find delimiter");
		} else {
			position = (uint8_t)(charPtr - selectedBuffer);
		}

		char *btDevice = &selectedBuffer[position + 1];
        logDebug("Selected bluetooth device is: %s", btDevice);
        btConnect(btDevice);
    }
}

static void wifiMenuRefreshCB(lv_event_t *e)
{
	(void) e;
	wifiMenuRefresh();
}

void wifiMenuRefresh(void)
{
	char currentSSIDbuffer[32];
	if (getCurrentSSID(currentSSIDbuffer, 32))
	{
		lv_dropdown_set_text(wifiMenu, LV_SYMBOL_WIFI);
	} else {
		lv_dropdown_set_text(wifiMenu, LV_SYMBOL_WIFI LV_SYMBOL_WARNING);
	}

	scanSSIDs();
    u_int32_t length = getSSIDlistSize();
    char ssidBuffer[length + 1];
    getSSIDs(ssidBuffer, length + 1);
    purgeSSIDs();
    lv_dropdown_set_options(wifiMenu, ssidBuffer);
}

static void wifiSelectionManagerCB(lv_event_t *e)
{
    lv_obj_t *_wifiMenu = lv_event_get_target(e);
    lv_obj_t *wifiMenuList = lv_dropdown_get_list(_wifiMenu);
    if (wifiMenuList == NULL)
    {
        logWarning("Could not fetch wifi menu list after value changed.");
    } else {
        logDebug("Wifi list exists after value changed");
        char selectedBuffer[MENU_OPTION_NAME_MAX];
        lv_dropdown_get_selected_str(_wifiMenu, selectedBuffer, MENU_OPTION_NAME_MAX);

        char *charPtr = strchr(selectedBuffer, WIFI_SSID_DELIMITER);
        uint8_t position = 0;
        if (charPtr == NULL)
        {
			logWarning("Parsing WiFI SSID did not find delimiter");
		} else {
			position = (uint8_t)(charPtr - selectedBuffer);
		}

		char *ssid = &selectedBuffer[position + 1];
        logDebug("Selected Wifi is: %s", ssid);
        makeWifiForm(ssid);
    }
}

void makeWifiForm(char *ssid)
{
    static char const * buttons[] = {APPLY_BUTTON_NAME, CANCEL_BUTTON_NAME, ""};

    lv_obj_t *wifiForm = lv_msgbox_create(NULL, "Enter Wifi Password", "", buttons, true);
    lv_obj_set_size(wifiForm, lv_pct(90), lv_pct(45));
    lv_obj_set_style_outline_color(wifiForm, lv_color_black(), LV_PART_MAIN);
    lv_obj_set_style_outline_width(wifiForm, 3, LV_PART_MAIN);
    lv_obj_set_align(wifiForm, LV_ALIGN_TOP_MID);
    lv_obj_set_pos(wifiForm, 0, lv_pct(5));

    // keyboard & text area
    lv_obj_t *passwordEntry = lv_textarea_create(wifiForm);
    lv_obj_set_width(passwordEntry, lv_pct(100));
    lv_textarea_set_one_line(passwordEntry, true);
    lv_textarea_set_password_mode(passwordEntry, true);
    lv_obj_set_style_outline_color(passwordEntry, lv_color_black(), LV_PART_MAIN);
    lv_obj_set_style_outline_width(passwordEntry, 3, LV_PART_MAIN);
    //lv_textarea_set_text(passwordEntry, "");

    lv_obj_t *keyboard = lv_keyboard_create(lv_layer_top());
    lv_obj_set_size(keyboard, lv_pct(100), lv_pct(50));
    lv_keyboard_set_textarea(keyboard, passwordEntry);

	wfd.keyboard = keyboard;
	wfd.ssid = malloc(strlen(ssid) + 1);
	strcpy(wfd.ssid, ssid);
    lv_obj_add_event_cb(wifiForm, wifirFormCb, LV_EVENT_VALUE_CHANGED, &wfd);
}

static void wifirFormCb(lv_event_t *e)
{
    lv_obj_t *formButtons = lv_event_get_target(e);
    uint16_t buttonId = lv_btnmatrix_get_selected_btn(formButtons);
    char const *buttonTxt = lv_btnmatrix_get_btn_text(formButtons, buttonId);

	struct wifiFormData *data = lv_event_get_user_data(e);
    if (strcmp(buttonTxt, APPLY_BUTTON_NAME) == 0)
    {
		logDebug("Attempting to connect to wifi");
		char const *password = lv_textarea_get_text(lv_keyboard_get_textarea(data->keyboard));
		connect(data->ssid, (char *)password);
	} else if (strcmp(buttonTxt, CANCEL_BUTTON_NAME) == 0)
	{
		logDebug("Wifi form close button pressed.");
	}

    lv_obj_del(data->keyboard);
    lv_msgbox_close(lv_event_get_current_target(e));
    data->keyboard = NULL;
    free(data->ssid);

    lv_event_send(wifiMenu, LV_EVENT_REFRESH, NULL);
	check_book_catalog();
}

void makeBookServerForm(void)
{
    static char const * buttons[] = {APPLY_BUTTON_NAME, CANCEL_BUTTON_NAME, ""};

    lv_obj_t *serverForm = lv_msgbox_create(NULL, "Type Server URL", "", buttons, true);
    lv_obj_set_size(serverForm, lv_pct(90), lv_pct(45));
    lv_obj_set_style_outline_color(serverForm, lv_color_black(), LV_PART_MAIN);
    lv_obj_set_style_outline_width(serverForm, 3, LV_PART_MAIN);
    lv_obj_set_align(serverForm, LV_ALIGN_TOP_MID);
    lv_obj_set_pos(serverForm, 0, lv_pct(5));

    // server list
    lv_obj_t *serverChoices = lv_dropdown_create(serverForm);
    lv_dropdown_set_options(serverChoices, "Calibre\nCustom");
    lv_dropdown_set_selected_highlight(serverChoices, true);
    lv_obj_add_event_cb(serverChoices, dropdownListStyleCB, LV_EVENT_READY, NULL);

    // keyboard & text area
    lv_obj_t *urlTextEntry = lv_textarea_create(serverForm);
    lv_obj_set_width(urlTextEntry, lv_pct(100));
    lv_textarea_set_one_line(urlTextEntry, true);
    lv_obj_set_style_outline_color(urlTextEntry, lv_color_black(), LV_PART_MAIN);
    lv_obj_set_style_outline_width(urlTextEntry, 3, LV_PART_MAIN);
    //lv_textarea_set_text(urlTextEntry, "http://192.168.1.193:8080/opds");

    lv_obj_t *keyboard = lv_keyboard_create(lv_layer_top());
    lv_obj_set_size(keyboard, lv_pct(100), lv_pct(50));
    lv_keyboard_set_textarea(keyboard, urlTextEntry);

    lv_obj_add_event_cb(serverForm, serverFormCb, LV_EVENT_VALUE_CHANGED, keyboard);
}

static void serverFormCb(lv_event_t *e)
{
    lv_obj_t *formButtons = lv_event_get_target(e);
    uint16_t buttonId = lv_btnmatrix_get_selected_btn(formButtons);
    char const *buttonTxt = lv_btnmatrix_get_btn_text(formButtons, buttonId);

	lv_obj_t *keyboard = lv_event_get_user_data(e);
    if (strcmp(buttonTxt, APPLY_BUTTON_NAME) == 0)
    {
		char const *serverURL = lv_textarea_get_text(lv_keyboard_get_textarea(keyboard));
		logDebug("Server URL is: %s", serverURL);
		storeBookserverURL(serverURL);
	} else if (strcmp(buttonTxt, CANCEL_BUTTON_NAME) == 0)
	{
		logDebug("Server form close button pressed.");
	}

    lv_obj_del(keyboard);
    lv_msgbox_close(lv_event_get_current_target(e));
}

bool storeBookserverURL(char const *serverURL)
{
	logDebug("Storing bookserver URL: %s", serverURL);
	char *cmd = getenv("ER_ADD_BOOKSERVER_TEMPLATE_EXEC");
    if (cmd == NULL)
    {
        logError("Command to add bookserver is undefined.");
        return false;
    }

    char *command = malloc((strlen(serverURL) + strlen(cmd) + 1)*sizeof(char));
    if (command == NULL)
    {
		logError("Failed to allocate memory for bookserver addition command");
		return false;
	}

    strcpy(command, cmd);
	strcat(command, serverURL);
	system(command);
	check_book_catalog();

	free(command);
	command = NULL;
	return true;
}

void clockCB(lv_timer_t *timer)
{
	logDebug("Clock text timer triggered");
	lv_obj_t *clock = (lv_obj_t *)timer->user_data;
	char *labelText = lv_label_get_text(clock);
	currentTime(labelText);
	lv_label_set_text(clock, labelText);
}

void batteryPercentRefresh(void)
{
	uint8_t percentInt = batteryChargePercent();
	char percentTxt[50];
	sprintf(percentTxt, "%u", percentInt);
	strcat(percentTxt, "%");
	if (percentInt >= 80)
	{
		strcat(percentTxt, LV_SYMBOL_BATTERY_FULL);
	} else if (60 <= percentInt && percentInt < 80)
	{
		// 3/4
		strcat(percentTxt, LV_SYMBOL_BATTERY_3);
	} else if (40 <= percentInt && percentInt < 60)
	{
		// 1/2
		strcat(percentTxt, LV_SYMBOL_BATTERY_2);
	} else if (20 <= percentInt && percentInt < 40)
	{
		// 1/4
		strcat(percentTxt, LV_SYMBOL_BATTERY_1);
	} else if (percentInt < 20)
	{
		// 0
		strcat(percentTxt, LV_SYMBOL_BATTERY_EMPTY);
	}
	lv_label_set_text(batteryLabel, percentTxt);
}

void batteryCB(lv_timer_t *timer)
{
	(void) timer;
	logDebug("Battery label timer triggered");
	batteryPercentRefresh();
}
