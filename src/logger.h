#ifndef ER_LOG_H 
#define ER_LOG_H


void logInfo(const char* message, ...);
void logWarning(const char* message, ...);
void logDebug(const char* message, ...);
void logError(const char* message, ...);

#endif // ER_LOG_H
