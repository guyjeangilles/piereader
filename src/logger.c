#include <stdarg.h>
#include <stdio.h>
#include "logger.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_RESET   "\x1b[0m"

void logFormat(const char* tag, const char* message, va_list args)
{
	printf("[%s] ", tag);
	vprintf(message, args);
	printf("\n");
}

void logInfo(const char* message, ...)
{
	va_list args;
	va_start(args, message);
	logFormat("INFO", message, args);
	va_end(args);
}

void logWarning(const char* message, ...)
{
	va_list args;
	va_start(args, message);
	logFormat(ANSI_COLOR_YELLOW "WARNING" ANSI_COLOR_RESET, message, args);
	va_end(args);
}

void logDebug(const char* message, ...)
{
	va_list args;
	va_start(args, message);
	logFormat("DEBUG", message, args);
	va_end(args);
}

void logError(const char* message, ...)
{
	va_list args;
	va_start(args, message);
	logFormat(ANSI_COLOR_RED "ERROR" ANSI_COLOR_RESET, message, args);
	va_end(args);
}
