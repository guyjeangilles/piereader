#ifndef _ER_WIFI_H
#define _ER_WIFI_H

#include <stdlib.h>
#include <stdbool.h>

#define WIFI_SSID_DELIMITER ':'

bool getSSIDs(char *buffer, u_int32_t bufferSize);
bool getCurrentSSID(char *buffer, u_int32_t bufferSize);
bool connect(char *ssid, char *pwd);
bool scanSSIDs(void);
void purgeSSIDs(void);
u_int32_t getSSIDlistSize(void);

#endif //_ER_WIFI_H
