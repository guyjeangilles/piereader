#ifndef _ER_SPI_H_
#define _ER_SPI_H_

#include <stdint.h>
#include <stdbool.h>

#define SPI_LSBFIRST 0
#define SPI_MSBFIRST 1

#define SPI_CS0 0
#define SPI_CS1 1

#define SPI_CLOCK_DIVIDER_128 128

#define SPI_MODE0 0b00
#define SPI_MODE1 0b01
#define SPI_MODE2 0b10
#define SPI_MODE3 0b11

bool spiOpen(void);
void spiClose(void);
void spiSetBitOrder(uint8_t order);
void spiSetMode(uint8_t mode);
void spiSetFrequency(uint16_t divider); // argument is clock divder that sets frequency
void spiSetChipSelect(uint8_t cs);
void spiSetChipSelectPolarity(uint8_t cs, uint8_t active);
uint8_t spiTransfer(uint8_t data);
void spiTransferN(uint8_t * buffer);

#endif //_ER_SPI_H_
