#ifndef _TPS61165_H_
#define _TPS61165_H_
#include <stdint.h>

void tps61165Init(void);
void tps61165SetBrightness(uint8_t dutyCycle);

#endif //_TPS61165_H_
