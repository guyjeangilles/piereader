#include "pwm.h"
#include "raspberrypi.h"

// `channel` is the PWM channel
// `value` is the numerator of the duty cycle if converted to a fraction.
void pwmSetValue(uint8_t channel, uint32_t value)
{
	rpiPWMsetValue(channel, value);
}

void pwmEnable(void)
{
	rpiPWMenableGPIO12();
	rpiPWMsetMode(0, 1, 1);

}

void pwmSetClock(uint16_t clock_divider)
{
	if (clock_divider == PWM_CLOCK_DIVIDER_32)
	{
		rpiPWMsetClock(RPI_PWM_CLOCK_DIVIDER_32);
		return;
	}
	rpiPWMsetClock(RPI_PWM_CLOCK_DIVIDER_16);
}

void pwmSetRange(uint8_t channel, uint32_t range)
{
	rpiPWMsetRange(channel, range);
}
