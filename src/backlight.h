#ifndef _BACKLIGHT_H_
#define _BACKLIGHT_H_

#include "lvgl/lvgl.h"

void lvKBacklightKeypadSetup(void);
lv_group_t *lvGetBacklightGroup(void);

#endif //_BACKLIGHT_H_
