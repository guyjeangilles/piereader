#include "spi.h"
#include "raspberrypi.h"

bool spiOpen(void)
{
	return rpiSpiOpen();
}
void spiClose(void)
{
	return rpiSpiClose();
}

void spiSetBitOrder(uint8_t order)
{
	if (order == SPI_LSBFIRST)
	{
		rpiSpiSetBitOrder(RPI_SPI_LSBFIRST);
		return;
	}
	rpiSpiSetBitOrder(RPI_SPI_MSBFIRST);
}

void spiSetMode(uint8_t mode)
{
	if (mode == SPI_MODE0)
	{
		rpiSpiSetMode(RPI_SPI_MODE0);
	} else if (mode == SPI_MODE1) {
		rpiSpiSetMode(RPI_SPI_MODE1);
	} else if (mode == SPI_MODE2) {
		rpiSpiSetMode(RPI_SPI_MODE2);
	} else {
		rpiSpiSetMode(RPI_SPI_MODE3);
	}
}

void spiSetFrequency(uint16_t divider) // argument is clock divder that sets frequency
{
	if (divider == SPI_CLOCK_DIVIDER_128){
		rpiSpiSetFrequency(RPI_SPI_CLOCK_DIVIDER_128);
	} else {
		rpiSpiSetFrequency(RPI_SPI_CLOCK_DIVIDER_512);
	}
}
void spiSetChipSelect(uint8_t cs){
	if (cs == SPI_CS0)
	{
		rpiSpiSetChipSelect(RPI_SPI_CS0);
		return;
	}
	rpiSpiSetChipSelect(RPI_SPI_CS1);
}

void spiSetChipSelectPolarity(uint8_t cs, uint8_t active)
{
	if (cs == SPI_CS0)
	{
		rpiSpiSetChipSelectPolarity(RPI_SPI_CS0, active);
		return;
	}
	rpiSpiSetChipSelectPolarity(RPI_SPI_CS1, active);
}

uint8_t spiTransfer(uint8_t data)
{
	return rpiSpiTransfer(data);
}

void spiTransferN(uint8_t *buffer){
	rpiSpiTransferN(buffer);
}
