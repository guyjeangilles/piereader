#include "touch.h"
#include "ft6336.h"
#include "logger.h"

static void lvTouchRead(lv_indev_drv_t * indev_drv, lv_indev_data_t * data);

lv_group_t *touchGroup;
lv_indev_t *indevTouch;

void lvTouchSetup(void)
{
    static lv_indev_drv_t indev_drv;
    ft6336Begin();

    // Register a touchscreen input device
    lv_indev_drv_init(&indev_drv);
    indev_drv.type = LV_INDEV_TYPE_POINTER;
    indev_drv.read_cb = lvTouchRead;
    indevTouch = lv_indev_drv_register(&indev_drv);

    // Create LVGL group for touchpad
    touchGroup = lv_group_create();
    lv_group_set_default(touchGroup);
    lv_indev_set_group(indevTouch, touchGroup);
}

// call back when screen is touched
static void lvTouchRead(lv_indev_drv_t * indev_drv, lv_indev_data_t * data)
{
    (void)indev_drv;

    static lv_coord_t last_x = 0;
    static lv_coord_t last_y = 0;

    if(ft6336IsTouched()) {
	uint16_t x, y;
	if (ft6336ReadTouch1X(&x) && ft6336ReadTouch1Y(&y))
	{
	    logDebug("Rotated touch point is (%u, %u)", y, LV_VER_RES - x);
	    data->point.x = (lv_coord_t)x;
	    data->point.y = (lv_coord_t)y;
	    last_x = data->point.x;
	    last_y = data->point.y;
	    data->state = LV_INDEV_STATE_PR;
	} else {
	    logWarning("Failed to read data from touchscreen");
	}
    } else {
	data->point.x = (lv_coord_t)last_x;
	data->point.y = (lv_coord_t)last_y;
	data->state = LV_INDEV_STATE_REL;
    }
}

lv_group_t *lvGetTouchGroup(void)
{
    return touchGroup;
}
