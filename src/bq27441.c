#include <stdbool.h>
#include "i2c.h"
#include "logger.h"
#include "bq27441.h"

#define BQ72441_I2C_ADDRESS 0x55

#define BQ27441_COMMAND_SOC 0x1C // Percent Charge

uint16_t bq27441readWord(uint16_t subAddress);
bool bq27441i2cReadBytes(uint8_t subAddress, uint8_t *buffer, uint8_t dataLen);

// state of charge, integer out of one hundred
uint16_t bq27441soc(void)
{
	return bq27441readWord(BQ27441_COMMAND_SOC);
}

uint16_t bq27441readWord(uint16_t subAddress)
{
	uint8_t data[2];
	bq27441i2cReadBytes((uint8_t)subAddress, data, 2);
	//return ((uint16_t) data[1] << (uint8_t) 8) | (uint16_t) data[0];
	uint8_t value = data[1] << 8;
	value |= (uint8_t) data[0];
	return value;
}

bool bq27441i2cReadBytes(uint8_t subAddress, uint8_t *buffer, uint8_t dataLen)
{
	if(!i2cInit())
	{
		logError("Failed to start I2C!");
		return false;
	}

	i2cSetTargetAddress(BQ72441_I2C_ADDRESS);
	i2cSetBaudrate(200000);
	i2cWrite((char *)&subAddress, sizeof(subAddress));
	i2cRead((char *)buffer, dataLen);
	i2cClose();
	return true;
}
