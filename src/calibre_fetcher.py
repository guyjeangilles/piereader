"""Download Book Catalog from Calibre OPDS Server."""
import argparse
import feedparser
import json
import logging
import os
import requests
from pathlib import Path
from urllib.parse import urlparse


class CalibreServerFetcher:
    """Download Library Content From Calibre Server."""
    def __init__(self):
        self._feed_urls = []
        self._book_path = ""

        # TODO don't hardcode this
        with open("/home/ereader/.config/piereader/config.json") as fin:
            config_dict = json.load(fin)
            self._book_path = config_dict["book_directory"]
            self._feed_urls = config_dict["bookservers"]

    def add_bookserver(self, url: str) -> None:
        """Add URL to list of bookservers."""
        parser_result = urlparse(url)
        if not all(parser_result.scheme and parser_result.netloc and parser_result.hostname):
            logging.error(f"{URL} is not valid. Not adding it to bookserver list")
            return

        # TODO don't hardcode this
        with open("/home/ereader/.config/piereader/config.json") as fin:
            config_dict = json.load(fin)
            config_dict["bookservers"].append(url)

        with open ("/home/ereader/.config/piereader/config.json.tmp", "w") as fout:
            fout.write(json.dumps(config_dict))

        os.rename("/home/ereader/.config/piereader/config.json.tmp", "/home/ereader/.config/piereader/config.json")

    def catalog_feed(self) -> list:
        """List of books in catalog."""
        books = []
        for url in self._feed_urls:
            feed = feedparser.parse(url)
            if not feed.entries:
                logging.info(f"No entries found in {url}.")
                continue

            catalog_url = feed.entries[1].links[0].href
            books += feedparser.parse(catalog_url).entries
        return books

    def has_new_books(self) -> bool:
        """Are there new books to download?"""
        local_books = {Path(book).stem for book in os.listdir(self._book_path)}
        catalog = {book.title for book in self.catalog_feed()}
        has_new_books = not local_books.issuperset(local_books.symmetric_difference(catalog))
        print(1, end="") if has_new_books else print(0, end="")
        return has_new_books

    def download_all(self) -> None:
        """Download every book in the catalog."""
        for item in self.catalog_feed():
            filetype = item.links[0].type
            if filetype == "application/epub+zip":
                extension = ".epub"
            elif filetype == "application/x-cbz":
                extension = ".cbz"
            elif filetype == "application/pdf":
                extension = ".pdf"
            else:
                logging.info(f"skipping unknown filetype {filetype}")
                continue

            filepath = "".join([self._book_path, item.title, extension])
            if os.path.exists(filepath):
                logging.info(f"already downloaded {item.title}")
                continue

            temp_filepath = "".join([self._book_path, ".", item.title, extension, ".tmp"])

            logging.debug(f"downloading {item.title}")
            book_url = item.links[0].href
            with open(temp_filepath, "wb") as file:
                response = requests.get(book_url)
                file.write(response.content)

            os.rename(temp_filepath, filepath)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "-c",
        "--check-new",
        action="store_true",
        help="Check if there are new books to download. 1 if new books, 0 if no new books.",
    )
    parser.add_argument(
        "-d",
        "--download",
        action="store_true",
        help="Download new books from server.",
    )
    parser.add_argument(
        "-a",
        "--add",
        type=str,
        help="Add a URL to bookserver list.",
    )

    args = parser.parse_args()

    fetcher = CalibreServerFetcher()
    if args.check_new:
        fetcher.has_new_books()

    if args.download:
        fetcher.download_all()

    if args.add:
        fetcher.add_bookserver(args.add)
