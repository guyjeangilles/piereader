#ifndef _ER_PROGRESSION_H_
#define _ER_PROGRESSION_H_

#include <stdint.h>
#include <stdbool.h>

enum Progression_Status {
  STATUS_OK = 0,
  FILE_READ_FAIL = -1,
  FILE_WRITE_FAIL = -2,
  FILE_RENAME_FAIL = -3,
  FILE_DATA_FAIL = -4,
};

typedef int8_t pr_progression_t;

uint32_t getPageNumber(char *bookFilepath);
bool increasePageNumber(char *bookFilepath);
bool decreasePageNumber(char *bookFilepath);

#endif // _ER_PROGRESSION_H_
