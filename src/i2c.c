#include "i2c.h"
#include "raspberrypi.h"

uint8_t returnI2Cresult(uint8_t result);

 bool i2cInit(void)
{
	return rpiI2Cinit();
}

void i2cSetTargetAddress(uint8_t address)
{
	rpiI2CsetTargetAddress(address);
}
void i2cSetBaudrate(uint32_t baudrate)
{
	rpiI2CsetBaudrate(baudrate); 
}

uint8_t i2cWrite(char const *buffer, uint32_t len)
{
	uint8_t result = rpiI2Cwrite(buffer, len);
	return returnI2Cresult(result);
}

uint8_t i2cRead(char *buffer, uint32_t len)
{
	uint8_t result = rpiI2Cread(buffer, len);
	return returnI2Cresult(result);
}

void i2cClose(void)
{
	rpiI2Cclose();
}

uint8_t returnI2Cresult(uint8_t result)
{
	if (result == RPI_I2C_REASON_OK)
	{
		return I2C_REASON_OK;
	} else if (result == RPI_I2C_REASON_ERROR_NACK)
	{
		return I2C_REASON_ERROR_NACK;
	} else if (result == RPI_I2C_REASON_ERROR_CLKT)
	{
		return I2C_REASON_ERROR_CLKT;
	} else if (result == RPI_I2C_REASON_ERROR_DATA)
	{
		return I2C_REASON_ERROR_DATA;
	} else if (result == RPI_I2C_REASON_ERROR_TIMEOUT)
	{
		return I2C_REASON_ERROR_TIMEOUT;
	} else
	{
		return I2C_ERROR_UNKNOWN;
	}
}
