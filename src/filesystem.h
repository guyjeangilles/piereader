#ifndef _ER_FILESYSTEM_H_
#define _ER_FILESYSTEM_H_

#include <stdint.h>

#define DEFAULT_DIRECTORY_SEPARATOR "/"
#define DEFAULT_HOME_DIR_NAME "home"
#define DEFAULT_USERS_PATH DEFAULT_DIRECTORY_SEPARATOR DEFAULT_HOME_DIR_NAME DEFAULT_DIRECTORY_SEPARATOR
#define DEFAULT_USER "pi"
#define DEFAULT_HOME_PATH DEFAULT_USERS_PATH DEFAULT_USER DEFAULT_DIRECTORY_SEPARATOR
#define DEFAULT_SUB_DIR_NAME "piereader"

enum Filesystem_Status {
  FILE_PARSE_OK = 0,
  FILE_PARSE_FAIL = -1,
  FILE_PARSE_EMPTY = -2,
};

typedef int8_t er_filesystem_t;

er_filesystem_t readJson(char *filepath, char *buffer);
er_filesystem_t searchJsonStr(char *content,  char const *searchKey, char **pValue);
er_filesystem_t searchJsonUInt(char *content,  char const *searchKey, uint32_t *pValue);
er_filesystem_t searchJsonFloat(char *content,  char const *searchKey, float *pValue);
void getHomepath(char *buffer);
bool endsWithSeparator(char const *directoryPath);

#endif // _ER_FILESYSTEM_H_
