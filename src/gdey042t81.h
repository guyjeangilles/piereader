#ifndef _GDEY042T81_H
#define _GDEY042T81_H

bool gdey042t81Setup(void);
void gdey042t81DisplayFast(uint8_t const *imageBuffer);
void gdey042t81PartialDisplay(uint8_t const *imageBuffer);
void gdey042t81SafeClose(void);
void gdey042t81InitFast(void);
void gdey042t81ClearFast(void);

#endif //_GDEY042T81_H
