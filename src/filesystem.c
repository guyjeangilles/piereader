#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <sys/stat.h>
#include "frozen.h"
#include "filesystem.h"
#include "logger.h"

er_filesystem_t readJson(char *filepath, char *buffer)
{
	logDebug("JSON file is: %s", filepath);
	struct stat st;
	if (stat(filepath, &st) != 0)
	{
		// json read fails if file is empty
		return FILE_PARSE_EMPTY;
	}

	strcpy(buffer, json_fread(filepath));
	if (buffer == NULL)
	{
		// failed to read json file
		return FILE_PARSE_FAIL;
	}
	return FILE_PARSE_OK;
}

er_filesystem_t searchJsonStr(char *content,  char const *searchKey, char **pValue)
{
	int readResult = json_scanf(content,(int)strlen(content), searchKey, pValue);
	if (readResult <= 0)
	{
		return FILE_PARSE_FAIL;
	}
	return FILE_PARSE_OK;
}

er_filesystem_t searchJsonUInt(char *content,  char const *searchKey, u_int32_t *pValue)
{
	int readResult = json_scanf(content,(int)strlen(content), searchKey, pValue);
	if (readResult <= 0)
	{
		return FILE_PARSE_FAIL;
	}
	return FILE_PARSE_OK;
}

er_filesystem_t searchJsonFloat(char *content,  char const *searchKey, float *pValue)
{
	int readResult = json_scanf(content,(int)strlen(content), searchKey, pValue);
	if (readResult <= 0)
	{
		return FILE_PARSE_FAIL;
	}
	return FILE_PARSE_OK;
}

void getHomepath(char *buffer)
{
	char const *homepath = getenv("HOME");
	if (homepath == NULL || strlen(homepath) == 0)
	{
		logDebug("Home environment variable does not exist or is empty");
		char const *user = getenv("USER");
		if (user == NULL || strlen(user) == 0)
		{
			logDebug("User environment variable does not exist or is empty");
			strcpy(buffer, DEFAULT_HOME_PATH);
			return;
		}
		strcat(buffer, DEFAULT_USERS_PATH);
		strcat(buffer, user);
		strcat(buffer, DEFAULT_DIRECTORY_SEPARATOR);
		return;
	}
	logDebug("HOME environment exists and is not empty");
	strcpy(buffer, homepath);
	if (!endsWithSeparator(homepath))
	{
		strcat(buffer, DEFAULT_DIRECTORY_SEPARATOR);
	}
}

bool endsWithSeparator(char const *directoryPath)
{
	return directoryPath[strlen(directoryPath) - 1] == '/';
}
