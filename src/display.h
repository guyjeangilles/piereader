#ifndef _ER_DISPLAY_H_
#define _ER_DISPLAY_H_

#define DISPLAY_X_RESOLUTION 400
#define DISPLAY_Y_RESOLUTION 300

#define ROTATED_DISPLAY_X_RESOLUTION 300
#define ROTATED_DISPLAY_Y_RESOLUTION 400

void displaySafeClose(void);
void lvDisplayInit(void);
void lvRegisterDisplay(void);
void displaySafeClose(void);

#endif // _ER_DISPLAY_H_
