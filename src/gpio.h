#ifndef _GPIO_H_
#define _GPIO_H_

#include <stdint.h>
#include <stdbool.h>

#define GPIO_INPUT 0
#define GPIO_OUTPUT 1

bool gpioInit(void);
bool gpioClose(void);
void gpioSetMode(uint32_t pin, uint8_t mode);
void gpioWrite(uint32_t pin, uint8_t value);
bool gpioRead(uint32_t pin);
void gpioPullDown(uint32_t pin);
void delayMs(uint32_t ms);

#endif //_GPIO_H_
