#include "bluetooth.h"
#include "logger.h"
#include "string.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#define ER_BT_DISCOVER_TEMPLATE_EXEC "sudo venv/bin/python src/bluetooth_discover.py -o "
#define BT_DEVICE_FILEPATH "btdevices.temp"
#define BT_DISCOVER_CMD ER_BT_DISCOVER_TEMPLATE_EXEC BT_DEVICE_FILEPATH
#define BT_INITIALIZE_CMD "rfkill unblock bluetooth && bluetoothctl agent on && bluetoothctl default-agent && bluetoothctl power on"
#define BT_PAIR_CMD "bluetoothctl pair "
#define BT_CONNECT_CMD "bluetoothctl connect "
#define BT_DISCONNECT_CMD "bluetoothctl disconnect "

void _btPair(char *address);
bool _btConnect(char *address);

void btInit(void)
{
    logDebug("Initializing bluetooth.");
    system(BT_INITIALIZE_CMD);
    // don't return status because command will error if BT is already on
}

bool btScan(void)
{
    if (system(BT_DISCOVER_CMD) != 0)
    {
        logError("Faield to scan bluetooth devices.");
        return false;
    }
    return true;
}

u_int32_t btGetDeviceListSize(void)
{
    FILE *filestream = fopen(BT_DEVICE_FILEPATH, "rb");

    if (filestream == NULL)
    {
        logError("Failed to find Bluetooth list size");
        return 0;
    }

    fseek (filestream, 0, SEEK_END);
    u_int32_t length = (u_int32_t)ftell (filestream);
    fclose(filestream);
    return length;
}

// each index will be the address and device name seperated by ";"
bool btDevices(char *buffer, u_int32_t bufferSize)
{
    FILE *filestream = fopen (BT_DEVICE_FILEPATH, "rb");

    char devices[bufferSize];
    u_int32_t readLength = (u_int32_t)fread(devices, 1, bufferSize, filestream);
    if ((bufferSize - 1) != readLength)
    {
        logError("Bytes read from Bluetooth filepath different than buffer size");
        return false;
    }

    fclose(filestream);
    devices[bufferSize] = '\0';
    strcpy(buffer, devices);
    return true;
}

bool btConnect(char *address)
{
    _btPair(address);
    return _btConnect(address);
}

bool btDisconnect(char *address)
{
    logDebug("Disconnecting from %s", address);
    char *cmd = malloc((strlen(BT_DISCONNECT_CMD) + strlen(address) + 1) * sizeof(char));
    strcpy(cmd, BT_DISCONNECT_CMD);
    strcat(cmd, address);
    bool result = system(cmd);
    free(cmd);
    return result;
}

void btPurge(void)
{
	remove(BT_DEVICE_FILEPATH);
}

void _btPair(char *address)
{
    logDebug("Pairing with %s", address);
    char *cmd = malloc((strlen(BT_PAIR_CMD) + strlen(address) + 1) * sizeof(char));
    strcpy(cmd, BT_PAIR_CMD);
    strcat(cmd, address);
    system(cmd);
    free(cmd);
    // don't return status because command will error if devices is already paired
}

bool _btConnect(char *address)
{
    logDebug("Connecting with %s", address);
    char *cmd = malloc((strlen(BT_CONNECT_CMD) + strlen(address) + 1) * sizeof(char));
    strcpy(cmd, BT_CONNECT_CMD);
    strcat(cmd, address);
    bool result = system(cmd);
    free(cmd);
    return result;
}
