#ifndef _BLUETOOTH_H_
#define _BLUETOOTH_H_

#include <stdint.h>
#include <stdbool.h>
#include <sys/types.h>

#define ER_BLUETOOTH_DEVICES_DELIMITER ';'

void btInit(void);
bool btScan(void);
u_int32_t btGetDeviceListSize(void);
// each index will be the address and device name seperated by ";"
bool btDevices(char *buffer, u_int32_t bufferSize);
void btPurge(void);
bool btConnect(char *address);
bool btDisconnect(char *address);

#endif // _BLUETOOTH_H_
