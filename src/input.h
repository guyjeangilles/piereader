#ifndef _EREADER_INPUT_H_
#define _EREADER_INPUT_H_

#include "lvgl/lvgl.h"

void lvInputSetup(void);
lv_group_t *lvGetInputGroup(void);

#endif // _EREADER_INPUT_H_
