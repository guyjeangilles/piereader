#include "wifi.h"
#include "logger.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <stdbool.h>

#define WIFI_SSID_FILEPATH "ssid.temp"
#define WIFI_SCAN_SSID_CMD "nmcli -g IN-USE,SSID dev wifi list > " WIFI_SSID_FILEPATH
#define WIFI_GET_CURRENT_SSID "iwgetid -r"
#define WIFI_CONNECT_CMD "nmcli dev wifi connect "

bool scanSSIDs(void)
{
    if (system(WIFI_SCAN_SSID_CMD) != 0)
    {
        logError("Failed to scan available SSIDs.");
        return false;
    }
    return true;
}

void purgeSSIDs(void)
{
    remove(WIFI_SSID_FILEPATH);
}

u_int32_t getSSIDlistSize(void)
{
    FILE * filestream = fopen (WIFI_SSID_FILEPATH, "rb");

    if (filestream == NULL)
    {
        logError("Failed to find SSID list size");
        return 0;
    }

    fseek (filestream, 0, SEEK_END);
    u_int32_t lenght = (u_int32_t)ftell (filestream);
    fclose(filestream);
    return lenght;
}

bool getSSIDs(char *buffer, u_int32_t bufferSize)
{
    FILE *filestream = fopen (WIFI_SSID_FILEPATH, "rb");

    char ssids[bufferSize];
    u_int32_t readLength = (u_int32_t)fread(ssids, 1, bufferSize, filestream);
    if ((bufferSize - 1) != readLength) // account for null character
    {
        logError("Bytes read from SSID filepath different than buffer size");
        return false;
    }

    fclose(filestream);
    ssids[bufferSize] = '\0';
    strcpy(buffer, ssids);
    return true;
}

bool getCurrentSSID(char *buffer, u_int32_t bufferSize)
{
    char *commandOutput = "";
    char buf[bufferSize];
    FILE *filestream;
    if ((filestream = popen(WIFI_GET_CURRENT_SSID, "r")) == NULL)
    {
        logError("Error opening current SSID pipe");
        return false;
    }

    while(fgets(buf, (int)bufferSize, filestream) != NULL)
    {
        commandOutput = buf;
    }

    if (pclose(filestream))
    {
        logError("Failed to get current SSID from filestream.");
        return false;
    }

    strcpy(buffer, commandOutput);
    return true;
}

bool connect(char *ssid, char *pwd)
{
    char *cmd = malloc((strlen(WIFI_CONNECT_CMD) + strlen(ssid) + strlen(" password ") + strlen(pwd) + 1) * sizeof(char));
    strcpy(cmd, WIFI_CONNECT_CMD);
    strcat(cmd, ssid);
    strcat(cmd, " password ");
    strcat(cmd, pwd);
    bool result = system(cmd);
    free(cmd);
    return result;
}
