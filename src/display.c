#include <signal.h>
#include <stdlib.h>
#include <stdbool.h>
#include "lvgl/lvgl.h"
#include "display.h"
#include "logger.h"
#include "gpio.h"

#ifdef GDEY042T81
    #include "gdey042t81.h"
#else
    #include "gdew042t2.h"
#endif

#define DISPLAY_ROW_LEN (DISPLAY_X_RESOLUTION / 8u)
#define BIT_SET(a, b) ((a) |= (1U << (b)))
#define BIT_CLEAR(a, b) ((a) &= ~(1U << (b)))

static bool displayInit(void);
void lvDisplayDisableUpdate(void);
static void lvDisplayFlush(lv_disp_drv_t *disp_drv, const lv_area_t * area, lv_color_t *color_p);
void lvSetPixelCallback(lv_disp_drv_t *disp_drv, uint8_t * buf, lv_coord_t buf_w, lv_coord_t x, lv_coord_t y, lv_color_t color, lv_opa_t opa);

uint8_t bufferCopy[DISPLAY_X_RESOLUTION * DISPLAY_Y_RESOLUTION];
volatile bool lvDisplayFlushEnabled = true;

void lvRegisterDisplay(void)
{
    static lv_disp_draw_buf_t draw_buf_dsc_1;
    static lv_color_t buf_1[DISPLAY_X_RESOLUTION * DISPLAY_Y_RESOLUTION];    
    lv_disp_draw_buf_init(&draw_buf_dsc_1, buf_1, NULL, DISPLAY_X_RESOLUTION * DISPLAY_Y_RESOLUTION);
    
    static lv_disp_drv_t disp_drv; 
    lv_disp_drv_init(&disp_drv);
    
    disp_drv.draw_buf = &draw_buf_dsc_1;

    disp_drv.hor_res = DISPLAY_X_RESOLUTION;
    disp_drv.ver_res = DISPLAY_Y_RESOLUTION;
    disp_drv.rotated = LV_DISP_ROT_270;

    disp_drv.flush_cb = lvDisplayFlush;
    disp_drv.set_px_cb = lvSetPixelCallback;
    disp_drv.direct_mode = 1;

    lv_disp_t * disp;
    disp = lv_disp_drv_register(&disp_drv);
    (void)disp;
}

void lvDisplayInit(void)
{
    if(!displayInit())
    {
        logError("LVGL failed to initialize display");
    }
}

void displaySafeClose(void)
{
    #ifdef GDEY042T81
        gdey042t81SafeClose();
    #else
        gdew042t2Init();
        gdew042t2Clear();
        gdew042t2Sleep();
        delayMs(2000);  // Must be at least 2 seconds
        gdew042t2Close();
    #endif
}

static bool displayInit(void)
{
    #ifdef GDEY042T81
        logDebug("Setting up GDEY042T81");
        if(!gdey042t81Setup())
        {
            logError("Failed to initialize display");
            return 0;
        }

        logDebug("Initializing GDEY042T81");
        gdey042t81InitFast(); //EPD init Fast
        logDebug("Clearing GDEY042T81");
        gdey042t81ClearFast();//EPD Clear
        logDebug("Cleared GDEY042T81");
        return 1;
    #else
        logDebug("Initializing GDEW042T2");
        if(!gdew042t2Init())
        {
            logError("Failed to initialize display");
            return 0;
        }

        gdew042t2Clear();
        delayMs(500);
        return 1;
    #endif
}

static void lvDisplayFlush(lv_disp_drv_t *disp_drv, const lv_area_t * area, lv_color_t *color_p)
{
    (void)area;
    (void)color_p;
    (void)disp_drv;
    logDebug("Flush area corners are (%u, %u) and (%u, %u)", area->x1, area->y1, area->x2, area->y2);
    lv_disp_t * disp = _lv_refr_get_disp_refreshing();
    logDebug("Number of invalid areas: %u", disp->inv_p);
    for (uint32_t i = 0; i <= disp->inv_p; i++)
    {
        if (disp->inv_area_joined[i] == 1)
        {
            logDebug("Invalid area corners are (%u, %u) and (%u, %u) where joined",  disp->inv_areas[i].x1, disp->inv_areas[i].y1, disp->inv_areas[i].x2, disp->inv_areas[i].y2);
        } else {
            logDebug("Invalid area corners are (%u, %u) and (%u, %u)", disp->inv_areas[i].x1, disp->inv_areas[i].y1, disp->inv_areas[i].x2, disp->inv_areas[i].y2);
        }
    }
    #ifdef GDEY042T81
        static uint8_t numPartialRefresh = 0;
        if (numPartialRefresh < 50)
        {
            logDebug("Partial refeshing whole display");
            gdey042t81PartialDisplay(bufferCopy);
            numPartialRefresh++;
        } else {
            logDebug("Full refresh display");
            gdey042t81InitFast();
            gdey042t81ClearFast();
            gdey042t81DisplayFast(bufferCopy);
            numPartialRefresh = 0;
        }
    #else
        gdew042t2Display(bufferCopy);
    #endif
    lv_disp_flush_ready(disp_drv); // inform LVGL we're ready to flush
}

void lvSetPixelCallback(lv_disp_drv_t * disp_drv, uint8_t * buf, lv_coord_t buf_w, lv_coord_t x, lv_coord_t y, lv_color_t color, lv_opa_t opa)
{
   // color is 1 pixel per byte instead of 8 pixels per byte
   // perform bit packing to make it the latter
   (void)disp_drv;
   (void)buf;
   (void)buf_w;
   (void)opa;

   lv_coord_t x_rot270, y_rot270;
   #ifdef GDEY042T81
        x_rot270 = -y;
        y_rot270 = DISPLAY_Y_RESOLUTION - x;
    #else
        x_rot270 = y;
        y_rot270 = DISPLAY_Y_RESOLUTION - 1 - x;
    #endif
   uint32_t byte_index_rot270 = (x_rot270 >> 3u) + (y_rot270 * (uint)DISPLAY_ROW_LEN);
   uint8_t bit_index_rot270 = x_rot270 & 0x07u;
   if (color.full) {
       BIT_SET(bufferCopy[byte_index_rot270], 7 - bit_index_rot270);
   } else {
       BIT_CLEAR(bufferCopy[byte_index_rot270], 7 - bit_index_rot270);
   }
}
