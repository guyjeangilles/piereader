#include <time.h>
#include <stdio.h>
#include <string.h>
#include "time.h"

void currentTime(char *buffer)
{
    time_t now = time(NULL);
    char clock[ER_CLOCK_BUF_SIZE];
    struct tm *tmp = localtime(&now);
    strftime(clock, sizeof(clock), "%I:%M %p", tmp);
    strcpy(buffer, clock);
}