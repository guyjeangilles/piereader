#ifndef _GDEW042T2_H_
#define _GDEW042T2_H_

#include <stdbool.h>
#include <stdint.h>

void gdew042t2Display(uint8_t *buffer);
bool gdew042t2Init(void);
void gdew042t2Clear(void);
void gdew042t2Sleep(void);
void gdew042t2Close(void);

#endif // _GDEW042T2_H_
