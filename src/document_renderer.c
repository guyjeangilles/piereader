#include <mupdf/fitz.h>
#include "document_renderer.h"
#include "logger.h"

float fontSize = 14.;

dr_render_status_t registerDocumentHandlers(fz_context *ctx);
dr_render_status_t resizeDocument(fz_context *ctx, fz_document *doc, float xRes, float yRes, float *widthScale, float *heightScale);

u_int32_t getPageCount(char *filepath, float xRes, float yRes)
{
	// make mupdf context
	fz_context *ctx = fz_new_context(NULL, NULL, FZ_STORE_UNLIMITED);
	if (!ctx)
	{
		logError("Cannot create MuPDF context");
		return 0;
	}

	registerDocumentHandlers(ctx);

	// open the document
    fz_document *doc;
    fz_try(ctx)
        doc = fz_open_document(ctx, (char const *)filepath);
	fz_catch(ctx)
	{
		logError("Cannot open MuPDF document: %s", fz_caught_message(ctx));
		fz_drop_context(ctx);
		return 0;
	}

	float widthScale = 1, heightScale = 1;
    resizeDocument(ctx, doc, xRes, yRes, &widthScale, &heightScale);

	int32_t pageCount;
	fz_try(ctx)
		pageCount = fz_count_pages(ctx, doc);
	fz_catch(ctx)
	{
		logError("Cannot count number of pages: %s", fz_caught_message(ctx));
		fz_drop_document(ctx, doc);
		fz_drop_context(ctx);
		return 0;
	}

	if (pageCount >= 0)
	{
		logDebug("Page count is: %u", pageCount);
		return (u_int32_t)pageCount;
	}
	logError("Page count is negative (%i)", pageCount);
	return 0;
}

// TODO return error statuses
dr_render_status_t fillImage(char *filepath, float xRes, float yRes, uint32_t pageNumber, uint8_t *imageBuffer)
{
    // make mupdf context
    fz_context *ctx = fz_new_context(NULL, NULL, FZ_STORE_UNLIMITED);
	if (!ctx)
	{
		logError("Cannot create MuPDF context");
		return RENDER_INIT_FAIL;
	}

	dr_render_status_t returnValue = registerDocumentHandlers(ctx);
	if (returnValue != RENDER_OK)
	{
		logError("Failed to register document handlers for %s", filepath);
		return returnValue;
	}

    // open the document
    fz_document *doc;

    fz_try(ctx)
        doc = fz_open_document(ctx, (char const *)filepath);
	fz_catch(ctx)
	{
		logError("Cannot open MuPDF document: %s", fz_caught_message(ctx));
		fz_drop_context(ctx);
		return RENDER_OPEN_FAIL;
	}

    //
    // Use MuPDF reflow if document is reflowable
    // otherwise (PDF, cbz, etc.) resize document to display size
    //
    float widthScale = 1, heightScale = 1;
    returnValue = resizeDocument(ctx, doc, xRes, yRes, &widthScale, &heightScale);
	if (returnValue != RENDER_OK)
	{
		logError("Failed to resize page for %s", filepath);
		return returnValue;
	}

    // render grayscale pixel map
    fz_pixmap *pix;
    fz_matrix ctm = fz_scale(heightScale, widthScale);
    fz_try(ctx)
        pix = fz_new_pixmap_from_page_number(ctx, doc, (int)pageNumber, ctm, fz_device_gray(ctx), 0);
    fz_catch(ctx)
    {
        logError("Cannot render MuPDF page: %s", fz_caught_message(ctx));
        fz_drop_document(ctx, doc);
        fz_drop_context(ctx);
        return RENDER_PAGE_FAIL;
    }

    // convert pixel data into LVGL format
    uint64_t i = 0;
    for (uint16_t y = 0; y < pix->h; ++y)
    {
        unsigned char *p = &pix->samples[y * pix->stride];
        for (uint16_t x = 0; x < pix->w; ++x)
        {
            imageBuffer[i] = 255 - p[0];
            i++;
            p += pix->n;
        }
    }

	returnValue = RENDER_OK;
	// Check if pixel map matches physical display resolution
    if (pix->w > xRes || pix->h > yRes)
    {
        logError("Pixel map (%d, %d) is larger than display (%f, %f)", pix->w, pix->h, (double)yRes, (double)xRes);
        returnValue = RENDER_RESOLUTION_FAIL;
    }

    fz_drop_pixmap(ctx, pix);
    fz_drop_document(ctx, doc);
    fz_drop_context(ctx);
    return returnValue;
}

dr_render_status_t registerDocumentHandlers(fz_context *ctx)
{
	fz_try(ctx)
		fz_register_document_handlers(ctx);
	fz_catch(ctx)
	{
		logError("Cannot register MuPDF document handlers: %s", fz_caught_message(ctx));
		fz_drop_context(ctx);
		return RENDER_HANDLERS_FAIL;
	}
	return RENDER_OK;
}

dr_render_status_t resizeDocument(fz_context *ctx, fz_document *doc, float xRes, float yRes, float *widthScale, float *heightScale)
{
    fz_try(ctx)
        if(fz_is_document_reflowable(ctx, doc))
        {
            // relayout document to display size
            fz_try(ctx)
                fz_layout_document(ctx, doc, xRes, yRes, fontSize);
            fz_catch(ctx)
            {
                logError("Cannot layout MuPDF document: %s", fz_caught_message(ctx));
                fz_drop_document(ctx, doc);
                fz_drop_context(ctx);
                return RENDER_REFLOW_FAIL;
            }
        } else {
            // scale document
            fz_rect rect;
            fz_try(ctx)
                rect = fz_bound_page(ctx, fz_load_page(ctx, doc, 0));
            fz_catch(ctx)
            {
                logError("Cannot find bounding box of MuPDF document: %s", fz_caught_message(ctx));
                fz_drop_document(ctx, doc);
                fz_drop_context(ctx);
                return RENDER_SCALE_FAIL;
            }

            // scale image to proper resolution
            float width = rect.x1 - rect.x0;
            float height = rect.y1 - rect.y0;

            *widthScale = (float)xRes/height;
            *heightScale = (float)yRes/width;
        }
    fz_catch(ctx)
	{
		logError("Failed to resize MuPDF document: %s", fz_caught_message(ctx));
		fz_drop_context(ctx);
		return RENDER_RESIZE_FAIL;
	}

	return RENDER_OK;
}
