#include "tps61165.h"
#include "gpio.h"
#include "pwm.h"
#include "logger.h"

#define PWM_PIN 12
#define PWM_CHANNEL 0
#define PWM_RANGE 128

void tps61165Reset(void);
void tps61165EnablePWM(void);

void tps61165Init(void)
{
	pwmEnable();
    pwmSetClock(PWM_CLOCK_DIVIDER_16);
    pwmSetRange(PWM_CHANNEL, PWM_RANGE);
    
    tps61165Reset();
    tps61165EnablePWM();
}

void tps61165SetBrightness(uint8_t dutyCycle)
{
	uint32_t pwm_value = (uint32_t)((dutyCycle * PWM_RANGE) / 100);
	pwmSetValue(PWM_CHANNEL, pwm_value);
}

void tps61165EnablePWM(void)
{	// Enable PWM dimming instead of Easy 1-Wire dimming
	// Must configure PWM before calling
	pwmSetValue(PWM_CHANNEL, PWM_RANGE);
	pwmSetValue(PWM_CHANNEL, 0);
	pwmSetValue(PWM_CHANNEL, PWM_RANGE);
	pwmSetValue(PWM_CHANNEL, 0);
}

void tps61165Reset(void)
{	// pull CTRL pin low for at least 2.5 seconds
	// must configure PWM before calling
	pwmSetValue(PWM_CHANNEL, 0);
	delayMs(3);
}
