#include "gdew042t2.h"
#include "gpio.h"
#include "spi.h"
#include "logger.h"

#define COMMAND 0
#define DATA 1

#define GDEW042T2_WIDTH 400
#define GDEW042T2_HEIGHT 300

#define	RESET_PIN 17
#define	DC_PIN 25
#define	CS_PIN  8
#define POWER_PIN 18
#define	BUSY_PIN 24

static const unsigned char gdew042t2_lut_vcom0[] = {
    0x00, 0x08, 0x08, 0x00, 0x00, 0x02,	
	0x00, 0x0F, 0x0F, 0x00, 0x00, 0x01,	
	0x00, 0x08, 0x08, 0x00, 0x00, 0x02,	
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 					
};

static const unsigned char gdew042t2_lut_ww[] = {
	0x50, 0x08, 0x08, 0x00, 0x00, 0x02,	
	0x90, 0x0F, 0x0F, 0x00, 0x00, 0x01,	
	0xA0, 0x08, 0x08, 0x00, 0x00, 0x02,	
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};

static const unsigned char gdew042t2_lut_bw[] = {
	0x50, 0x08, 0x08, 0x00, 0x00, 0x02,	
	0x90, 0x0F, 0x0F, 0x00, 0x00, 0x01,	
	0xA0, 0x08, 0x08, 0x00, 0x00, 0x02,	
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};

static const unsigned char gdew042t2_lut_wb[] = {
	0xA0, 0x08, 0x08, 0x00, 0x00, 0x02,	
	0x90, 0x0F, 0x0F, 0x00, 0x00, 0x01,	
	0x50, 0x08, 0x08, 0x00, 0x00, 0x02,	
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
};

static const unsigned char gdew042t2_lut_bb[] = {
	0x20, 0x08, 0x08, 0x00, 0x00, 0x02,	
	0x90, 0x0F, 0x0F, 0x00, 0x00, 0x01,	
	0x10, 0x08, 0x08, 0x00, 0x00, 0x02,	
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	
};

void gdew042t2Reset(void);
void gdew042t2SendCommand(uint8_t command);
void gdew042t2SendData(uint8_t data);
void gdew042t2WaitBusy(void);
void gdew042t2SetLUT(void);
void gdew042t2TurnOnDisplay(void);

void gdew042t2Display(uint8_t *buffer)
{
    uint16_t width, height;
    width = (GDEW042T2_WIDTH % 8 == 0)? (GDEW042T2_WIDTH / 8 ): (GDEW042T2_WIDTH / 8 + 1);
    height = GDEW042T2_HEIGHT;

	gdew042t2SendCommand(0x10);
    for (uint16_t j = 0; j < height; j++) {
        for (uint16_t i = 0; i < width; i++) {
            gdew042t2SendData(0x00);
        }
    }

    gdew042t2SendCommand(0x13);
    for (uint16_t j = 0; j < height; j++) {
        for (uint16_t i = 0; i < width; i++) {
            gdew042t2SendData(buffer[i + j * width]);
        }
    }

	gdew042t2SendCommand(0x12);		 //DISPLAY REFRESH
	delayMs(10);
    gdew042t2TurnOnDisplay();
}

bool gdew042t2Init(void)
{	
	// setup GPIO
	gpioSetMode(RESET_PIN, GPIO_OUTPUT);
	gpioSetMode(DC_PIN, GPIO_OUTPUT);
    gpioSetMode(CS_PIN, GPIO_OUTPUT);
	gpioSetMode(POWER_PIN, GPIO_OUTPUT);
	gpioSetMode(BUSY_PIN, GPIO_INPUT);

	gpioWrite(CS_PIN, 1);
    gpioWrite(POWER_PIN, 1);

	// configure SPI
	if (!spiOpen())
	{
		logError("Failed to intialize spi");
		return 0;
	}
	spiSetBitOrder(SPI_MSBFIRST);
	spiSetMode(SPI_MODE0);
	spiSetFrequency(SPI_CLOCK_DIVIDER_128);
	spiSetChipSelect(SPI_CS0);
	spiSetChipSelectPolarity(SPI_CS0, 0);

	// Initialize display
	gdew042t2Reset();
	gdew042t2SendCommand(0x01);			//POWER SETTING 
	gdew042t2SendData(0x03);	// TODO #define          
	gdew042t2SendData(0x00);
	gdew042t2SendData(0x2b);  
	gdew042t2SendData(0x2b);

	gdew042t2SendCommand(0x06);         //boost soft start
	gdew042t2SendData(0x17);		//A
	gdew042t2SendData(0x17);		//B
	gdew042t2SendData(0x17);		//C       

	gdew042t2SendCommand(0x04);
	gdew042t2WaitBusy();

	gdew042t2SendCommand(0x00);			//panel setting
	gdew042t2SendData(0xbf);		//KW-bf   KWR-2F	BWROTP 0f	BWOTP 1f


	gdew042t2SendCommand(0x30);			
	gdew042t2SendData (0x3c);      	// 3A 100HZ   29 150Hz 39 200HZ	31 171HZ

	gdew042t2SendCommand(0x61);			//resolution setting
	gdew042t2SendData(0x01);        	 
	gdew042t2SendData(0x90);	 //400	
	gdew042t2SendData(0x01);	 //300
	gdew042t2SendData(0x2c);	   

	gdew042t2SendCommand(0x82);			//vcom_DC setting  	
	gdew042t2SendData (0x12);	

	gdew042t2SendCommand(0X50);
	gdew042t2SendData(0x97);

	gdew042t2SetLUT();
	return 1;
}

void gdew042t2Clear(void)
{
	uint16_t width, height;
    width = (GDEW042T2_WIDTH % 8 == 0)? (GDEW042T2_WIDTH / 8 ): (GDEW042T2_WIDTH / 8 + 1);
    height = GDEW042T2_HEIGHT;

    gdew042t2SendCommand(0x10);
    for (uint16_t j = 0; j < height; j++) {
        for (uint16_t i = 0; i < width; i++) {
            gdew042t2SendData(0xFF);
        }
    }

    gdew042t2SendCommand(0x13);
    for (uint16_t j = 0; j < height; j++) {
        for (uint16_t i = 0; i < width; i++) {
            gdew042t2SendData(0xFF);
        }
    }

	gdew042t2SendCommand(0x12);		 //DISPLAY REFRESH
	delayMs(1);
    gdew042t2TurnOnDisplay();
}

void gdew042t2Sleep(void)
{
	gdew042t2SendCommand(0x50); // DEEP_SLEEP
    gdew042t2SendData(0XF7);

    gdew042t2SendCommand(0x02); // POWER_OFF
    gdew042t2WaitBusy();

    gdew042t2SendCommand(0x07); // DEEP_SLEEP
    gdew042t2SendData(0XA5);
}

void gdew042t2Close(void)
{
	gpioWrite(CS_PIN, 0);
	gpioWrite(POWER_PIN, 0);
	gpioWrite(DC_PIN, 0);
	gpioWrite(RESET_PIN, 0);

	spiClose();
}

void gdew042t2Reset(void)
{
	for (uint8_t i = 0; i < 3; i++)
	{
		gpioWrite(RESET_PIN, 0);
		delayMs(10);
		gpioWrite(RESET_PIN, 1);
		delayMs(10);
	}
}

void gdew042t2SendCommand(uint8_t command)
{
	gpioWrite(DC_PIN, COMMAND);
	gpioWrite(CS_PIN, 0); // FIXME let spi driver do this?
	spiTransfer(command);
	gpioWrite(CS_PIN, 1);
}

void gdew042t2SendData(uint8_t data)
{
	gpioWrite(DC_PIN, DATA);
	gpioWrite(CS_PIN, 0); // FIXME let spi driver do this?
	spiTransfer(data);
	gpioWrite(CS_PIN, 1);
}

void gdew042t2WaitBusy(void)
{
	//logDebug("GDEW042T2 busy");
	gdew042t2SendCommand(0x71); //TODO #define commands
	while(gpioRead(BUSY_PIN) == 0) // TODO #define Low == IDLE, HIGH == busy
	{
		gdew042t2SendCommand(0x71);
		delayMs(100);
	}
	//logDebug("GDEW042T2 idle");
}

void gdew042t2SetLUT(void)
{
	uint8_t count;
	gdew042t2SendCommand(0x20); // TODO #define
	for(count=0;count<36;count++)	     
		{gdew042t2SendData(gdew042t2_lut_vcom0[count]);}

	gdew042t2SendCommand(0x21); // TODO #define
	for(count=0;count<36;count++)	     
		{gdew042t2SendData(gdew042t2_lut_ww[count]);}   
	
	gdew042t2SendCommand(0x22); // TODO #define
	for(count=0;count<36;count++)	     
		{gdew042t2SendData(gdew042t2_lut_bw[count]);} 

	gdew042t2SendCommand(0x23); // TODO #define
	for(count=0;count<36;count++)	     
		{gdew042t2SendData(gdew042t2_lut_wb[count]);} 

	gdew042t2SendCommand(0x24); // TODO #define
	for(count=0;count<36;count++)	     
		{gdew042t2SendData(gdew042t2_lut_bb[count]);}
}

void gdew042t2TurnOnDisplay(void)
{
    gdew042t2SendCommand(0x12);
    delayMs(100);
    gdew042t2WaitBusy();
}
