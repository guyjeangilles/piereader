#include "gpio.h"
#include "raspberrypi.h"
#include "logger.h"

bool gpioInit(void){
	logDebug("Initializing GPIO.");
	return rpiGpioInit();
}

bool gpioClose(void){
	return rpiGpioClose();
}

void gpioSetMode(uint32_t pin, uint8_t mode){
	return rpiGpioSetMode((uint8_t)pin, mode);
}

void gpioWrite(uint32_t pin, uint8_t value)
{
	return rpiGpioWrite((uint8_t)pin, value);
}

bool gpioRead(uint32_t pin)
{
	return rpiGpioRead((uint8_t)pin);
}

void gpioPullDown(uint32_t pin)
{
	rpiGpioPullDown((uint8_t)pin);
}

void delayMs(uint32_t ms){
	return rpiDelay(ms);
}
