# piEreader
[**NO UPDATES PLANNED FOR THIS PROJECT**](https://hackaday.io/project/192366/log/231297-the-last-update)
![piEreader HAT and carrier boards](media/piEreader_boards.jpg)

## Sponsored by

[![PCBWay logo](https://s3-eu-west-1.amazonaws.com/tpd/logos/54695d4a00006400057b939d/0x0.png)](https://pcbway.com)

## As Seen On

[![hackster io logo](media/hackster-io-logo.png)](https://www.hackster.io/news/guyrandy-jean-gilles-piereader-is-a-chunky-3d-printed-epaper-book-reading-proof-of-concept-1b6845e9494c)

[![hackaday logo](https://images.crunchbase.com/image/upload/c_lpad,h_256,w_256,f_auto,q_auto:eco,dpr_1/v1476330656/ynl6g4z7kw487nwmsztq.png)](https://hackaday.io/project/192366-floss-book-serving-system)

## Introduction

piEreader is a proof-of-concept, open source, and DIY e-reader designed for the Raspberry Pi (more hardware compatibility planned).  It supports EPUB, MOBI, CBZ, PDF, and many more! The previous versions of piEreader:

- [`v1`](https://gitlab.com/guyjeangilles/piereader/-/tree/v1?ref_type=heads) cobbled together breakout boards. It is the minimalist of viable products

- [`v2`](https://gitlab.com/guyjeangilles/piereader/-/tree/v2?ref_type=heads) implemented a dedicated PCB following the [Raspberry Pi HAT spec](https://github.com/raspberrypi/hats)

- [`v3`](https://gitlab.com/guyjeangilles/piereader/-/tree/v3?ref_type=heads) ported the GUI to [LVGL](lvgl.io), enabled book server sync with [Calibre](https://calibre-ebook.com/)

- [`v4`](https://gitlab.com/guyjeangilles/piereader/-/tree/v4?ref_type=heads) manufactured carrier board for the [compute module 4 form factor](https://www.raspberrypi.com/products/compute-module-4/)

- [`v5`](https://gitlab.com/guyjeangilles/piereader/-/tree/v5?ref_type=heads) added touchscreen, front-light, and battery power to project

The ultimate goal of this project is to prototype an open source and self-hostable eBook reading system. That is, a book lover can modify the physical e-reader to their needs, can modify the source code to their needs, and remotely fetch books onto the physical e-reader whether it be from a third-party or self-hosted book server.

I started this project in January 2023 using Raspberry Pi boards I had laying around. Due to the supply shortage I started exploring other boards like the [Pine64-LTS](https://pine64.com/product/pine-a64-lts/), [SOQUARTZ](https://pine64.com/product/soquartz-2gb-compute-module-w/), [Core 3566](https://www.waveshare.com/core3566.htm), etc. Thanks to the ubiquitous 2x20 pin header form factor, I was able to port [`v2`](https://gitlab.com/guyjeangilles/piereader/-/tree/v2?ref_type=heads) of the project to the Pine64-LTS in [this repo](https://gitlab.com/guyjeangilles/pinereader/) with no hardware changes. I suspect I could do the same with other boards. The Raspberry Pi supply chain has improved as of writing this, but in an effort to keep the design open, an additional goal is to make the project configurable for different hardware platforms.

## Hardware

Production ready files are in the `Fabrication Files` folder. A bill of materials with part numbers is available in the same directory. You can generate your own production ready files with the project files in the `KiCad` folder.

Due to fine pitch components, you will want to order the PCB pre-assembled or buy a stencil. You'll also want to use double sided tape to secure the e-Paper display and battery to the PCB.

**NOTE: YOU WILL NEED TALL HEADERS FOR THE PCB TO FIT ON B(+) FORM FACTOR BOARDS**

Schematics for both carrier board and HAT designs are in the root directory of this repository.

## Software

Clone and initialize submodules using `git`. The project also relies on [MuPDF](https://mupdf.com)  being installed in `/usr/local/lib/`. Steps to install MuPDF are in [the library's documentation](https://mupdf.readthedocs.io/en/latest/quick-start-guide.html#linux). The [`bcm2385`](https://www.airspayce.com/mikem/bcm2835/) needs to be installed as well.

Run `sudo make` from the root directory to build the code. By default, the code will build for the [GDEW042T2](https://www.buy-lcd.com/products/42inch-eaper-display-400x300-spi-interface-and-demo-board-despi) display. To build the code for the [GDEY042T81-F02](https://www.buyepaper.com/products/42-inch-e-paper-display-fast-update-morochrome-spi-e-ink-with-touch-and-front-light-gdey042t81-ft02) touchscreen assembly, run `sudo make clean && sudo make DISPLAY=GDEY042T81 INPUT=TOUCH`.

Install python dependencies with the following commands run from the project's root directory:

```
python -m venv venv
source venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

Then execute the code from the repo's root directory with `sudo -E --preserve-env=USER bash src/start.bash`.

To make the hardware connect to your Calibre server, edit `$XDG_CONFIG_HOME/piereader/config.json`.

```
{
    "book_directory": "/home/ereader/Bookshelf/",
    "bookservers": [
        "www.your-opds-server.com"
    ]
}
```

### Software Troubleshooting

Note the project uses SPI and I2C to communicate with peripherals so be sure to enable those protocols in the Raspberry Pi OS. Also, to use PWM with the BCM2835 library, you'll have to comment out the following line in `/boot/config.txt`.

```
dtparam=audio=on
```

Also, the software requires `nmcli` and `python3-bluez` installed to run.

## Acknowledgements

- Special thanks to  [Jose Castillo ](https://github.com/joeycastillo) of [The Open Book](https://github.com/joeycastillo/The-Open-Book) project for releasing the design files. Doing so significantly eased the development of this project.

- Thank you to [Waveshare](https://www.waveshare.com/) for releasing firmware for their ePaper displays. Much of which is incorporated into this project.

- Thanks to [Good Display](https://www.good-display.com/) for their vendor code and shipping samples for their touchscreen assemblies.
