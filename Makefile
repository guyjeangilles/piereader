# Project config
PROJECT 			?= piEreader
MAKEFLAGS 			:= -j $(shell nproc)
SRC_EXT      		:= c
OBJ_EXT				:= o
CC 					?= gcc

# Base Directories
SRC_DIR				:= ./src
EXTERNAL_DIR		:= $(SRC_DIR)/external
WORKING_DIR			:= ./build
BUILD_DIR			:= $(WORKING_DIR)/obj
BIN_DIR				:= $(WORKING_DIR)/bin

# LVGL
LVGL_DIR_NAME ?= lvgl
LVGL_DIR ?= ${EXTERNAL_DIR}
include $(LVGL_DIR)/$(LVGL_DIR_NAME)/lvgl.mk

# Compiler flags
WARNINGS 			:= -Wall -Wextra -Wshadow -Wdouble-promotion \
						-Wformat=2 -Wundef -fno-common -fstack-usage \
						-Wconversion -Wpadded
CFLAGS 				:= -O0 -g3 -Os -ffunction-sections -fdata-sections \
						-Wl,--gc-sections $(WARNINGS)

# Includes
INC 				:= -I./ -I$(EXTERNAL_DIR)
LDLIBS	 			:= -lm -lbcm2835

# Source files and objects
SRCS 				:= $(shell find $(SRC_DIR) -type f -name '*.c' -not -path '*/\.*')
ASRCS				+= /usr/local/lib/libmupdf.a
ASRCS				+= /usr/local/lib/libmupdf-third.a
OBJECTS    			:= $(patsubst $(SRC_DIR)%,$(BUILD_DIR)/%,$(SRCS:.$(SRC_EXT)=.$(OBJ_EXT)))

# Readability variables
BIN 				:= $(BIN_DIR)/main
COMPILE				= $(CC) $(CFLAGS) $(INC)

ifeq ($(DISPLAY), GDEY042T81)
	DEFINES			:= -D $(DISPLAY)
endif

ifeq ($(INPUT), TOUCH)
	DEFINES			+= -D $(INPUT)
endif

all: default

$(BUILD_DIR)/%.$(OBJ_EXT): $(SRC_DIR)/%.$(SRC_EXT)
	@echo 'Building project file: $<'
	@mkdir -p $(dir $@)
	@$(COMPILE) $(DEFINES) -c -o "$@" "$<"

default: $(OBJECTS)
	@mkdir -p $(BIN_DIR)
	$(CC) -o $(BIN) $(OBJECTS) $(ASRCS) $(LDFLAGS) ${LDLIBS}
clean:
	rm -rf $(WORKING_DIR)
